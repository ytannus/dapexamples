pragma solidity ^0.4.17;
contract RPSGame {
	address owner;

	enum Choices { None, Rock, Paper, Scissors }
	string private constant SEED = "h4ckm3plz!";

	struct Move {
	    address addr;
	    bytes32  hiddenChoice;
  	}

  	Move[2] public players; //There are only 2 players
  	uint playerIndex;
  	uint gameFee;

  	address winner;
  	bool draw;
  	mapping(address => uint) balances;

	bool playable; //If game is opened or closed (played by 2 players)

	address RockPaperScissorsAddress;


	event LogGameCreated(uint _fee);
	event LogPlay(address _sender, uint _index);
	event LogCheckResult(address _winner);
	event LogPay(address _winner, address _sender, uint _amount);

	function RPSGame(uint _fee, address _owner)
		public
	{
		owner = _owner;
		gameFee = _fee;
		playable = true;
		draw = false;
		winner = owner;
		LogGameCreated(_fee);
	}

	function play(uint _index, uint _choice)
	//_index: player1=1,player2=2
		public
		payable
	{
		assert(msg.value == gameFee);
		require(_index == 1 || _index == 2); //only 2 players allowed
		require(_choice >= 0 && _choice < 4);

		players[_index -1].addr = msg.sender;
		players[_index -1].hiddenChoice = getHash(msg.sender,_choice);

		if (_index != 1) //first player, game is available
		{
			playable = false;
			checkResult();
		}

		LogPlay(msg.sender, _index);
	}

	function checkResult()
		public
	{
		assert(players[0].addr != 0x0);
		assert(players[1].addr != 0x0);

		if (players[0].hiddenChoice == players[1].hiddenChoice) {
			draw = true;
		} else if (
           (players[0].hiddenChoice == getHash(players[0].addr,uint(Choices.Rock))
		 && players[1].hiddenChoice == getHash(players[1].addr,uint(Choices.Scissors)))
         || (players[0].hiddenChoice == getHash(players[0].addr,uint(Choices.Paper))
		 && players[1].hiddenChoice == getHash(players[1].addr,uint(Choices.Rock)))
		 || (players[0].hiddenChoice == getHash(players[0].addr,uint(Choices.Scissors))
		 && players[1].hiddenChoice == getHash(players[1].addr,uint(Choices.Paper)))
		   )
		{
			winner = players[0].addr;//first player
		} else {
			winner = players[1].addr; //second player
		}

		balances[winner] = gameFee * 2;
		LogCheckResult(winner);
	}

	function pay()
		public
	{
		assert(winner == msg.sender);
		assert(balances[winner] > 0);

		balances[winner] = 0;
		winner.transfer(gameFee * 2);
		LogPay(winner,msg.sender, gameFee * 2);
	}

	function getHash(address _player, uint _choice)
		internal
		pure
		returns (bytes32)
	{
		return keccak256(_player,SEED,_choice);
	}

	function getGameData()
		constant
		public
		returns (bool, address, bool, address)
	{
		return (playable,winner,draw,owner);
	}

	function ()
		public
	{
		revert();
	}
}