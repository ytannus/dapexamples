pragma solidity ^0.4.17;
import "./RPSGame.sol";

contract RockPaperScissors {

	uint public constant FEE = 0.1 ether;

	address[] RPSgames;

	event LogNewGame(address _game, address _sender);

	function RockPaperScissors()
		public
	{
	}

	function newGame()
		public
	{
		address newRPSGame = new RPSGame(FEE, msg.sender);
		RPSgames.push(newRPSGame);
		LogNewGame(newRPSGame, msg.sender);
	}

	function getGameList()
		constant
		public
		returns (address[])
	{
		return RPSgames;
	}
}

