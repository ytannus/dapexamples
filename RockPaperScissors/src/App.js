import React, { Component } from 'react'
import RockPaperScissorsContract from '../build/contracts/RockPaperScissors.json'
import RPSGameContract from '../build/contracts/RPSGame.json'
import getWeb3 from './utils/getWeb3'

import './css/oswald.css'
import './css/open-sans.css'
import './css/pure-min.css'
import './App.css'

import { NewGameForm } from './components/NewGameForm';
import { GameForm } from './components/GameForm';
import { GameCard } from './components/GameCard';


class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      web3: null,
      RockPaperScissorsInstance: null,
      FEE: 0,
      RPSGame: null,
      gameList: null,
      playableGames: null,
      nonPlayableGames: null,
      AccountBalance: 0
    }
    this.handleNewGame = this.handleNewGame.bind(this)
    this.handlePlayGame = this.handlePlayGame.bind(this)
    this.handleLootGame = this.handleLootGame.bind(this)
  }

  componentWillMount() {
    // Get network provider and web3 instance.
    // See utils/getWeb3 for more info.

    getWeb3
    .then(results => {
      const contract = require('truffle-contract')
      this.setState({
        web3: results.web3,
        RockPaperScissors: contract(RockPaperScissorsContract),
        RPSGame: contract(RPSGameContract)
      })

      // Instantiate contract once web3 provided.
      this.instantiateContract()
    })
    .catch(() => {
      console.log('Error finding web3.')
    })
  }

  instantiateContract() {
    const contract = require('truffle-contract')
    const rockPaperScissors = contract(RockPaperScissorsContract)
    rockPaperScissors.setProvider(this.state.web3.currentProvider)

    const RPSGame = contract(RPSGameContract)
    RPSGame.setProvider(this.state.web3.currentProvider)
    this.setState({RPSGame: RPSGame})

    // Declaring this for later so we can chain functions on SimpleStorage.
    var rockPaperScissorsInstance

    // Get accounts.
    this.state.web3.eth.getAccounts((error, accounts) => {
      if (error != null) {
       console.log(error);
       alert("There was an error fetching your accounts.");
       return;
      }

      if (accounts.length === 0) {
        alert("Couldn't get any accounts! Make sure your Ethereum client is configured correctly.");
        return;
      }

      this.setState({ origin: accounts[0] })
      this.setState({ accounts: accounts })

      rockPaperScissors.deployed().then((instance) => {
        rockPaperScissorsInstance = instance
        this.setState({RockPaperScissorsInstance: rockPaperScissorsInstance})
        //Get all existing games
        this.getGameList()

        return instance.FEE.call()
      }).then((fee)=>{
        this.setState({FEE: fee.toNumber()}) //in WEI
        return this.getBalance(this.state.accounts[0])
      }).then((value) => {
        this.setState({AccountBalance: value})
      })
    })
  }

  async getGameList() {
      await this.state.RockPaperScissorsInstance.getGameList.call().then((gameList)=>{
        this.setState({gameList: gameList})
      }).catch((e) => {
        console.log(e)
      })
      this.updateGamesStatus()
  }

  async updateGamesStatus() {
    let playableGames = []
    let nonPlayableGames = []

    for(var i=0;i < this.state.gameList.length;i++){
      let address = this.state.gameList[i]
      let data
      await this.state.RPSGame.at(address).then((instance)=>{
        return instance.getGameData.call()
      }).then((res)=>{
        data = res
        return(this.getBalance(address))
      }).then((balance) => {
        data.unshift(address)
        data.push(balance)

        if (data[1]) {
          playableGames.push(data)
        } else {
          nonPlayableGames.push(data)
        }
      }).catch((e) => {
        console.log(e)
      })
    }
    this.setState({playableGames: playableGames})
    this.setState({nonPlayableGames: nonPlayableGames}) 
  }

  async NewGame(choice) {
      await this.state.RockPaperScissorsInstance.newGame({from: this.state.accounts[0], gas: 900000}).then((tx)=>{
        return tx.logs[0].args._game  
      }).then((gameAddress)=>{
         this.PlayGame(gameAddress,choice,1) //first plater move
      }).catch((e) => {
        console.log(e)
      })
  }

  async PlayGame(gameAddress,choice,playerIndex)
  {
    await this.state.RPSGame.at(gameAddress).then((game)=>{
      //PLAY GAME
      return game.play(playerIndex,choice,{from: this.state.accounts[0], value: this.state.FEE, gas: 900000})
    }).then((tx)=>{
      //GET STATUS AND WINNER
      return (this.state.RPSGame.at(gameAddress).getGameData.call())
    }).then((data)=>{
      //PAY ONLY IF IS A NON PLAYABLE GAME
      //also pays to owner if result is draw
      if (!data[0]) { 
        return (this.state.RPSGame.at(gameAddress).pay({from: this.state.accounts[0]}))
      } else {
        return false
      }
    }).then((tx)=>{
      //UPDATE ACCOUNT BALANCE
      return this.getBalance(this.state.accounts[0])
    }).then((value) => {
      this.setState({AccountBalance: value})
    }).catch((e) => {
      console.log(e)
    })
    this.updateGamesStatus()
  }

  async getBalance(addr) {
    const promisify = (inner) => new Promise((resolve, reject) => inner((err, res) => { if (err) { reject(err) } resolve(res) }))
    const getBalance = (addr, at) => promisify(cb => this.state.web3.eth.getBalance(addr, at, cb))
    const bigNumberBalance = await getBalance(addr)
    return this.state.web3.fromWei(bigNumberBalance.toNumber(),"ether")
  }

  async LootGame(address)
  {
    await this.state.RPSGame.at(address).then((game)=>{
      return game.loot({from: this.state.accounts[0], gas: 900000})
    }).then((tx) =>{
      console.log(tx)
    }).catch((e) => {
      console.log(e)
    })
  }

  handleNewGame(event){
    let choice = event.target.choice.value
    this.NewGame(choice)
    this.getGameList()
  }
  handlePlayGame(event){
    let address = event.target.address.value
    let choice = event.target.choice.value
    this.PlayGame(address,choice,2) //second player move
  }
  handleLootGame(event){
    let address = event.target.address.value
    this.LootGame(address) //second player move
  }


  render() {
    return (
      <div className="App">
        <nav className="navbar pure-menu pure-menu-horizontal">
            <a href="#" className="pure-menu-heading pure-menu-link">Rock Paper Scissors</a>
            <span className="white-label">Game Fee: {this.state.FEE} [WEI]</span> - 
            <span className="white-label">Your balance: {this.state.AccountBalance}</span>
        </nav>

        <main className="container">
          <div className="pure-g">
            <div className="pure-u-1-2">
              <h1>New game!</h1>
              <NewGameForm handleNewGame={this.handleNewGame}/>
            </div>
            <div className="pure-u-1-2">
            <h1>Games Available</h1>
              {
                this.state && this.state.playableGames &&
                this.state.playableGames.map((game,i) => {
                  if(game) {
                  return <GameForm key={i} address={this.state.playableGames[i][0]} handlePlayGame={this.handlePlayGame}/>
                  } else {
                    return false;
                  }
                })
              }
            </div>
            <div className="pure-u-1-1">
            <h1>Played Games</h1>
            <table className="pure-table">
              <thead>
                <tr>
                  <th>Game</th>
                  <th>Winner</th>
                </tr>
              </thead>
              <tbody>
              {
                this.state && this.state.nonPlayableGames &&
                this.state.nonPlayableGames.map((game,i) => {
                  if(game) {
                  return (<GameCard
                            key={i}
                            address={this.state.nonPlayableGames[i][0]}
                            winner={this.state.nonPlayableGames[i][2]}
                            draw={this.state.nonPlayableGames[i][3]}
                            owner={this.state.nonPlayableGames[i][4]}
                            balance={this.state.nonPlayableGames[i][5]}
                            sender={this.state.accounts[0]}
                            handleLootGame={this.handleLootGame}/>
                    )
                  } else {
                    return false;
                  }
                })
              }
              </tbody>
              </table>
            </div>
          </div>
        </main>
      </div>
    );
  }
}

export default App
