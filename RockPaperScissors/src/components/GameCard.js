import React from 'react'
import '../css/pure-min.css'
import '../App.css'

export class GameCard extends React.Component {
	constructor(props) {
    super(props)
      this.state = {
        winnerLabel: null
      }
  	}

  componentWillMount() {
    if (this.props.draw) {
      this.setState({winnerLabel: "Draw"})
    } else if (this.props.winner === this.props.sender) {
      this.setState({winnerLabel: "You!"})
    } else {
      this.setState({winnerLabel: this.props.winner})
    }
  }

	render() {
    return (
      <tr>
          <td>{this.props.address}</td>
          <td>{this.state.winnerLabel}</td>
      </tr>
  )
  }
}