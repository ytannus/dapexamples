import React from 'react'
import '../css/pure-min.css'
import '../App.css'

export class GameForm extends React.Component {
	constructor(props) {
    super(props)
      this.state = {
      }
      this.handleSubmit = this.handleSubmit.bind(this)
  	}
  handleSubmit(event) {
    event.preventDefault()
    this.props.handlePlayGame(event)
  }
	render() {
    return (
      <div>
      <form onSubmit={this.handleSubmit} className="pure-form">
        {this.props.address}
        <select name="choice">
          <option value="1">Rock</option>
          <option value="2">Paper</option>
          <option value="3">Scissors</option>
        </select>
        <input type="hidden" name="address" value={this.props.address}/>
        <input type="hidden" name="playerIndex" value="2"/>
        <button className="pure-button pure-button-primary">Play</button>
      </form>
      </div>
  )
  }
}