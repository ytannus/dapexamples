import React from 'react'
import '../css/pure-min.css'
import '../App.css'

export class NewGameForm extends React.Component {
	constructor(props) {
    super(props)
      this.state = {
      }
      this.handleSubmit = this.handleSubmit.bind(this)
  	}
	handleSubmit(event) {
		event.preventDefault()
		this.props.handleNewGame(event)
	}
	render() {
    return (
    	<div>
    	<form onSubmit={this.handleSubmit} className="pure-form">
       <div className="pure-control-group">
        Your Choice:
        <div className="pure-control-group">
        <label className="pure-radio">
          <input type="radio" name="choice" value="1"/> Rock
        </label>
        <label className="pure-radio">
          <input type="radio" name="choice" value="2"/> Paper
        </label>
        <label className="pure-radio">
          <input type="radio" name="choice" value="3"/> Scissors
        </label>
      </div>
      </div>
    	  <button className="pure-button pure-button-primary">Start a New Game</button>
    	</form>
    	</div>
	)
	}
}