module.exports = {
  networks: {
    development: {
      host: "127.0.0.1",
      port: 8545,
      network_id: "*", // Match any network id
    },
    kibercloud: {
      host: "10.0.1.27",
      port: 8545,
      network_id: "42",
      gas: 3000000
    }
  }
};
