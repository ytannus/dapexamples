# Splitter

Splits and send ether from one account to another two

## Dependencies
Node 8.x
```bash
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt update -y && apt install nodejs build-essential -y
```

Truffle
```bash
sudo npm install truffle ganache-cli -g
```

A working EVM (geth or ganache-cli)


## Installation
Clone the repository
```bash
git clone <REPO>
```

Alternative download development environment from:
[https://hub.docker.com/r/ytannus/truffle-env/]

### Author
Yery Tannus - <ytannus@gmail.com>
