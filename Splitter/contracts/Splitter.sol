pragma solidity ^0.4.17;

contract Splitter {
	address owner;
	bool enabled;

	mapping (address => uint) public balances;

   	event LogSplit(address rec1, address rec2, uint amount);
    event LogWithdraw(address rec, uint amount);
    event LogKill(address rec);

    modifier enabledNeeded {
        require(enabled);
        _;
    }

    modifier onlyOwner {
        require(msg.sender == owner);
        _;
    }

   	function Splitter()
   		public
   	{
	    owner = msg.sender;
	    enabled = true;
   	}
    
   	function split(address rec1, address rec2)
   		payable
   		enabledNeeded
   		public
   	{
	   	assert(enabled);
	    assert(msg.value > 0);
	    require(rec1 != 0 && rec2 != 0);

	    uint half = msg.value / 2;
	    balances[rec1] += half;
	    balances[rec2] += msg.value - half;
	    LogSplit(rec1, rec2, msg.value);
	}

    function withdraw()
    	enabledNeeded
    	public
    {
    	assert(balances[msg.sender] > 0);

        uint amount = balances[msg.sender];
        balances[msg.sender] = 0;
        msg.sender.transfer(amount);
        LogWithdraw(msg.sender, amount);
    }

    function kill()
    	enabledNeeded
    	onlyOwner
    	public
    {
    	enabled=false;
    	LogKill(msg.sender);
    	selfdestruct(owner);
    }

    function isEnabled()
    	constant
    	public
    	returns (bool)
    {
    	return enabled;
    }

    function ()
    	public
    {
        revert();
    }
}