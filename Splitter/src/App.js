import React, { Component } from 'react'
import SplitterContract from '../build/contracts/Splitter.json'
import getWeb3 from './utils/getWeb3'

import './css/oswald.css'
import './css/open-sans.css'
import './css/pure-min.css'
import './App.css'
import './css/splitter.css'

import { SetDestinationsForm } from './components/SetDestinationsForm';
import { SplitForm } from './components/SplitForm';
import { KillForm } from './components/KillForm';
import { AccountCard } from './components/AccountCard';

class App extends Component {

  constructor(props) {
    super(props)

    this.state = {
      web3: null,
      Splitter: null,
      SplitterAddress: 0,
      SplitterBalance: 0,
      Enabled: true,
      origin: 0,
      dest1: 0,
      dest2: 0,
      originBalance: 0,
      dest1Balance: 0,
      dest2Balance: 0
    }

    this.handleSetDestinations = this.handleSetDestinations.bind(this)
    this.handleSplit = this.handleSplit.bind(this)
    this.handleKill = this.handleKill.bind(this)
  }

  componentWillMount() {
    // Get network provider and web3 instance.
    // See utils/getWeb3 for more info.

    getWeb3
    .then(results => {
      const contract = require('truffle-contract')
      this.setState({
        web3: results.web3,
        Splitter: contract(SplitterContract)
      })

      // Instantiate contract once web3 provided.
      this.instantiateContract()

    })
    .catch(() => {
      console.log('Error finding web3.')
    })
  }

  instantiateContract() {
    /*
     * SMART CONTRACT EXAMPLE
     *
     * Normally these functions would be called in the context of a
     * state management library, but for convenience I've placed them here.
     */

    //const contract = require('truffle-contract')
    //const splitter = contract(SplitterContract)
    this.state.Splitter.setProvider(this.state.web3.currentProvider)

    // Declaring this for later so we can chain functions on Splitter.
    var splitterInstance

    // Get accounts.
    this.state.web3.eth.getAccounts((error, accounts) => {
      if (error != null) {
       console.log(error);
       alert("There was an error fetching your accounts.");
       return;
      }

      if (accounts.length === 0) {
        alert("Couldn't get any accounts! Make sure your Ethereum client is configured correctly.");
        return;
      }

      this.setState({ origin: accounts[0] })
      this.setState({ accounts: accounts })

      this.state.Splitter.deployed().then((instance) => {
        this.setState({SplitterInstance: instance})
        splitterInstance = instance
        this.setState({SplitterAddress: instance.address})
        this.isEnabled()
        this.updateBalances()
        return splitterInstance
      })
    }).catch((e) => {
      console.log(e)
    })
  }

  updateBalances()
  {
    this.getBalance(this.state.origin).then((balance) => {
      this.setState({originBalance: balance})
      return this.getBalance(this.state.dest1).then((dest1Balance) => {
        this.setState({dest1Balance: dest1Balance})
        return this.getBalance(this.state.dest2).then((dest2Balance) => {
          this.setState({dest2Balance: dest2Balance})
          return this.getBalance(this.state.SplitterAddress).then((splitterBalance) =>{
            this.setState({SplitterBalance: splitterBalance})
          })
        })
      })
    }).catch((e) => {
      console.log(e)
    })
  }

  async getBalance(addr) {
    const promisify = (inner) => new Promise((resolve, reject) => inner((err, res) => { if (err) { reject(err) } resolve(res) }))
    const getBalance = (addr, at) => promisify(cb => this.state.web3.eth.getBalance(addr, at, cb))
    const bigNumberBalance = await getBalance(addr)
    return this.state.web3.fromWei(bigNumberBalance.toNumber(),"ether")
  }

  handleSetDestinations(event){
    this.setState({dest1: event.target.dest1.value})
    this.setState({dest2: event.target.dest2.value})
    this.updateBalances()
  }

  async withdraw(address) {
    if(this.state.Enabled){
      await this.state.Splitter.deployed().then((instance)=>{
        return instance.withdraw({from: address, gas:90000})
      }).then((tx) => {
        //console.log(tx)
      }).catch((e) => {
      console.log(e)
      })
    }
  }

  async split(amount) {
    await this.state.Splitter.deployed().then((instance)=>{
      return instance.split(this.state.dest1,this.state.dest2,{from: this.state.origin, value: amount})
    }).then((tx) => {
      //console.log(tx)
      return this.withdraw(this.state.dest1.toString())
    }).then((tx) => {
      return this.withdraw(this.state.dest2)
    }).catch((e) => {
      console.log(e)
    })
    this.updateBalances()
  }

  async kill() {
      this.setState({Enabled: false});
      await this.state.Splitter.deployed().then((instance)=>{
        return instance.kill({from: this.state.origin})
      }).then((tx) => {
        //console.log(tx)
      }).catch((e) => {
        console.log(e)
      })
  }

  async isEnabled() {
      await this.state.Splitter.deployed().then((instance)=>{
        return instance.isEnabled.call()
      }).then((enabled) => {
        //console.log(enabled)
        this.setState({Enabled: enabled});
      }).catch((e) => {
        console.log(e)
      })
  }

  handleSplit(event){
    this.split(event.target.amount.value)
  }

  handleKill(event){
    this.kill()
  }

  render() {
    if(this.state.Enabled && this.state.SplitterAddress){
    return (
      <div className="App">

        <nav className="navbar pure-menu pure-menu-horizontal">
            <a href="#" className="pure-menu-heading pure-menu-link">Splitter</a>
        </nav>

        <main className="container">

          

          <SetDestinationsForm
            dest1={this.state.dest1}
            dest2={this.state.dest2}
            dest1Balance={this.state.dest1Balance}
            dest2Balance={this.state.dest2Balance}
            handleSetDestinations={this.handleSetDestinations}
          />

          <AccountCard
            name="Splitter Contract"
            address={this.state.SplitterAddress}
            balance={this.state.SplitterBalance}
          />
          <br/>
          <AccountCard
            name="Origin Account"
            address={this.state.origin}
            balance={this.state.originBalance}
          />
  
          <SplitForm
            dest1={this.state.dest1}
            dest2={this.state.dest2}
            handleSplit={this.handleSplit}
          />
          
          <AccountCard
            name="Destination 1"
            address={this.state.dest1}
            balance={this.state.dest1Balance}
          />

          <AccountCard
            name="Destination 2"
            address={this.state.dest2}
            balance={this.state.dest2Balance}
          />
          <hr/>
          <KillForm handleKill={this.handleKill}/>
        </main>
      </div>
    );
   }else{
    return(
        <div className="App">

        <nav className="navbar pure-menu pure-menu-horizontal">
            <a href="#" className="pure-menu-heading pure-menu-link">Splitter</a>
        </nav>

        <main className="container">
        <h1>Splitter is Disabled</h1>
        You should migrate the contract to create a new one, for example:
        <code>
          truffle migrate --reset --compile-all
        </code>
        </main>
        </div>
    )}
  }
}

export default App
