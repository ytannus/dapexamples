import React from 'react'
import '../css/pure-min.css'
import '../App.css'
import '../css/AccountCard.css'

import Blockies from 'react-blockies'

export class AccountCard extends React.Component {
	constructor(props) {
    super(props)
      this.state = {
      }
  	}
	render() {
    if (this.props.address) {
      return (
        <div className="card">

          <Blockies
              blockies={this.props.address} //string content to generate icon
              size={10} // blocky icon size
              style={{width:16, height:16}} // style of the view will wrap the icon
              scale={3}
              seed={this.props.address}
          />

          <div className="small-text"><span className="title">Name:</span> {this.props.name}</div>
          <div className="small-text"><span className="title">Address:</span> {this.props.address}</div>
          <div className="small-text"><span className="title">Balance:</span> {this.props.balance} (ethers)</div>
        </div>
  	  )
    } else {
      return (<div></div>)
    }
	}
}