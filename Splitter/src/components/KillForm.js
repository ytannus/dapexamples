import React from 'react'
import '../css/pure-min.css'
import '../App.css'

export class KillForm extends React.Component {
	constructor(props) {
    super(props)
      this.state = {
      }
      this.handleSubmit = this.handleSubmit.bind(this)
  	}
	handleSubmit(event) {
		event.preventDefault()
		this.props.handleKill(event)
	}
	render() {
    return (
    	<div>
    	<form onSubmit={this.handleSubmit} className="pure-form">
    	  <button className="pure-button pure-button-warning">Kill Splitter</button>
    	</form>
    	</div>
	)
	}
}