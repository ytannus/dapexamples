import React from 'react'
import '../css/pure-min.css'
import '../App.css'
import '../css/splitter.css'

export class SetDestinationsForm extends React.Component {

	constructor(props) {
    super(props)
      this.state = {
        setReceiversStatus: ''
      }
      this.handleInputChange = this.handleInputChange.bind(this)
      this.handleSubmit = this.handleSubmit.bind(this)
  	}

	handleInputChange(event) {
	    const target = event.target;
	    const value = target.type === 'checkbox' ? target.checked : target.value;
	    const name = target.name;

	    this.setState({
	      [name]: value
	    });
	}

	handleSubmit(event) {
		event.preventDefault()
		this.setState({setReceiversStatus: "Receivers defined"})
		this.props.handleSetDestinations(event)
	}

	shortenAddress(address) {
		let str = address.toString();
		let numberOfchars = 6;
		return str.substring(0,numberOfchars)+"..."+str.substring(str.length - numberOfchars,str.length)
	}

	render() {
    if(this.props.dest1 && this.props.dest2) {
      return(<div></div>)
    } else {
    return (
    	<div className="small-text">
    	<h3>Destinations</h3>
    	    
    	<form onSubmit={this.handleSubmit} className="pure-form">
    	<label htmlFor="receiver1" className="text-xsmall">Destination Account 1</label>
    	<input
               className="inline-edit"
               id="dest1"
               name="dest1"
               onChange={this.handleInputChange}
               required
        /><br/>
        <label htmlFor="receiver2" className="text-xsmall">Destination Account 2</label>
    	<input
               className="text-xsmall"
               id="dest2"
               name="dest2"
               onChange={this.handleInputChange}
               required
        /><br/>
        <button className="pure-button pure-button-primary">Set addresses</button><br/>
        <span className="text-xsmall">{this.state.setReceiversStatus}</span><br/>
    	</form>
      <hr/>
    	</div>
    )
  }
  }
}
