import React from 'react'
import '../css/pure-min.css'
import '../App.css'
import '../css/splitter.css'

export class SplitForm extends React.Component {
	constructor(props) {
    super(props)
      this.state = {
      }
      this.handleSubmit = this.handleSubmit.bind(this)
  	}

	handleSubmit(event) {
		event.preventDefault()
		this.props.handleSplit(event)
	}
	render() {
    if(this.props.dest1 && this.props.dest2) {
    return (
    	<div>
    	<form onSubmit={this.handleSubmit} className="pure-form">
    	Amount in Wei: <input
               className="text-xsmall"
               type="number"
               id="amount"
               name="amount"
               required
        />
        <button className="pure-button pure-button-primary">Split</button><br/>
    	</form>
    	</div>
    )} else {return (<div></div>)}
	}
}