const Splitter = artifacts.require("./Splitter.sol");

contract('Splitter', function(accounts) {
    it("starts with 0 for deployer", function() {
        return Splitter.deployed()
            .then(function(instance) {
                return instance.balances(accounts[0]);
            })
            .then(function(balance) {
                assert.strictEqual(balance.toNumber(), 0, "should start with nothing");
            });
    });

    describe("split", function() {
        let splitter;
        beforeEach("deploy fresh", function() {
            return Splitter.new({ from: accounts[0] })
                .then(function(created) {
                    splitter = created;
                });
        });
        it("should split 2 weis", function() {
            return splitter.split.call(
                accounts[1], accounts[2],
                { from: accounts[0], value: 2 })
                .then(function(successful) {
                    assert.isTrue(successful);
                    return splitter.split(
                        accounts[1], accounts[2],
                        { from: accounts[0], value: 2 });
                })
                .then(function(txObject) {
                    assert.strictEqual(txObject.logs[0].args.rec1, accounts[1]);
                    assert.strictEqual(txObject.logs[0].args.rec2, accounts[2]);
                    assert.strictEqual(txObject.logs[0].args.amount.toNumber(), 2);
                    return splitter.balances(accounts[0]);
                })
                .then(function (balance0) {
                    assert.strictEqual(balance0.toNumber(), 0);
                    return splitter.balances(accounts[1]);
                })
                .then(function (balance1) {
                    assert.strictEqual(balance1.toNumber(), 1);
                    return splitter.balances(accounts[2]);
                })
                .then(function (balance2) {
                    assert.strictEqual(balance2.toNumber(), 1);
                });
        });
    });
});