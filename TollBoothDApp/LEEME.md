# Project Title

## TollBooths Payment DAap
### Final Exam - Final Project - Step 2
#### ETH-18 Certified Online Ethereum Developer Course

## Getting Started

These instructions will get you a copy of this DApp project up and running on your local machine for development and testing purposes.

#### Contracts Implementation Diagram
![graph](graph.png)


### Prerequisites

This DApp was made using [truffle-vagrant-exam](https://git.academy.b9lab.com/ETH-18-course-repo/truffle-vagrant-exam) vangrant machine from [B9lab](http://b9lab.com/), that includes:

* OS: Ubuntu 14.04.5 LTS
* Framework: truffle@3.4.11
* Development Node: ganache-cli@6.0.3

You need to run a local EVM node.
For local development environment ensure that you have enough gas to deploy contracts, for example set limit to 10000000 

```sh
$ ganache-cli -l 10000000
```

### Installing

Clone this repository
```sh
$ git clone <this-repo>
$ cd <this-repo>
```

then install node modules dependencies:

```sh
$ npm install --save
```

Optional, you can build it using:
```sh
$ npm run build
```

## Running the tests

### Scenarios test:

Run scenarios tests executing:
```sh
$ truffle test test/scenarios.js
```

In summary of the test scenarios is as follows:

* Scenario 1: Route price is exact deposited amount, no refund
* Scenario 2: Route price is more than deposited amount, no refund
* Scenario 3: Route price is less than deposited amount, refunds
* Scenario 4: Vehicle deposits more than Base price and Route Price is equal to base Price, refunds
* Scenario 5: Vehicle deposits more than Base Price, Route price is unknown. Refunds after route price is sets and is lower than deposited amount
* Scenario 6: RoutePrice is unknown, 2 vehicles passed, after the price is established refunds to them 

### Running Full Tests

To run full tests just execute:
```sh
$ truffle test
```

## Compile and Deploy

Ensure that your local node is running and your account is unlocked.
To compile and deploy contracts to local EVM node run:

```sh
$ truffle migrate --reset --compile --all
```

## Run DApp
For local development environment use:
```sh
$ npm run dev
```

For production run:
```sh
$ php -S 0.0.0.0 -t ./build
```

## Usage

To access to the DApp point your browser to (http://localhost:8000)

At upper-right corner you will find the list of the accounts deployed in the EVM node.
You can change anytime what account you need to use selecting it from the list:

![Welcome](/images/welcome1.png)

There you will see the active account and its balance:

![Welcome](/images/welcome2.png)

Then select the new account to use from the list:

![Accounts](/images/welcome3.png)

To access to developed functionalities just click in the upper menu:
![Main Menu](/images/mainmenu.png)

## Regulator's Page

In this page you can:

### Set Vehicle Type

Add a Vehicle Type entering the Vehicle Address and the Type.

![setVehicleType](/images/regulator1.png)

* Vehicle Address need to be a valid address
* Type need to be an positive integer

### Create New Operator

You can create TollBoothOperators:

![setVehicleType](/images/regulator2.png)

* Enter the Owner of the TollBoothOperator to be created
* Enter the amount to be required to vehicle ingress to the road

## Operator's Page

First, You need to set what TollBoothOperator you need to use entering its address:
![setOperator](/images/operator0.png)

### Add TollBooth

To allow vehicles transit through the road you need to add TollBooths for entering or exits

![addTollBooth](/images/operator1.png)

* Need to be a valid address

### Set Route Price

You can set the price for routes between two TollBooths

![setRoutePrice](/images/operator2.png)

* Entry and Exit Booth need to be a valid Address
* Price value in Weis

### Set Multiplier

Multiplier to be applied to vehicle when transit

![setMultiplier](/images/operator3.png)

* VehicleType need to be setted by Regulator before
* Multiplier is a positive integre

## Vehicle's Page
First, You need to set what TollBoothOperator you need to use entering its address:
![setOperator](/images/operator0.png)

### Enter Road

Vehicles can report to operator that are entering to the road paying the access
![enterRoad](/images/vehicle1.png)

* Ingress the amount of weis you will pass to operator allows you to enter to the road
* Enter the entry booth where you are
* Set an uniq secret number to be used to record your transit at exit booth later

_We are using an integer as secret for UI simplicity, internally DApp converts it to a hashed secret_

### Show History

Also, vehicle, can get its transit historic records, for example:

![enterRoad](/images/vehicle2.png)



## TollBooth's Page
First, You need to set what TollBoothOperator you need to use entering its address:
![setOperator](/images/operator0.png)

### Report Exit Road
When a Vehicle exits you can record this action sending the uniq secret number given before by vehicle at entry booth
![setOperator](/images/tollbooth1.png)

## Author

* **Yery Tannus**  - [ytannus@itaum.com](mailto:ytannus@itaum.com)

## Acknowledgments

* IT Sandwish and Bar
