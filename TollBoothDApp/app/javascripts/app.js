// Import the page's CSS. Webpack will know what to do with it.
import "../stylesheets/app.css";

// Import libraries we need.
import { default as Web3} from 'web3';
import { default as contract } from 'truffle-contract';
import { default as Promise } from 'bluebird';

// Import our contract artifacts and turn them into usable abstractions.
import regulator_artifacts from '../../build/contracts/Regulator.json'
import operator_artifacts from '../../build/contracts/TollBoothOperator.json'

// Utils
const isAddress = require("../../utils/isAddress.js");
const toBytes32 = require("../../utils/toBytes32.js");
const randomIntIn = require("../../utils/randomIntIn.js");

var Regulator = contract(regulator_artifacts);
var TollBoothOperator = contract(operator_artifacts);

// The following code is simple to show off interacting with your contracts.
// As your needs grow you will likely need to change its form and structure.
// For application bootstrapping, check out window.addEventListener below.
var accounts;
var account;
var accountBalance;
const gas = 3000000;
let regulatorInstance, operatorInstance;
var operatorAddress;

var newAccountSelected;

window.App = {
  start: function() {
    var self = this;
    Regulator.setProvider(web3.currentProvider);
    TollBoothOperator.setProvider(web3.currentProvider);

    // Get the initial account balance so it can be displayed.
    web3.eth.getAccounts(function(err, accs) {
      if (err != null) {
        alert("There was an error fetching your accounts.");
        return;
      }

      if (accs.length == 0) {
        alert("Couldn't get any accounts! Make sure your Ethereum client is configured correctly.");
        return;
      }

      accounts = accs;
      self.setStatus("status","Welcome");
      self.promisifyWeb3(web3);
      account = accounts[0];
      self.setAccountLabel(account);
      self.setBalanceLabel(account);

      self.initAccountList();
    });
  },
  initAccountList: function() {
    for(var i=0;i<accounts.length;i++) {
      var newOption = document.createElement("option");
      newOption.value = i;
      newOption.text = accounts[i];
      if (account == accounts[i]) {newOption.selected = true;}
      var select = document.getElementById("newAccountId");
      select.appendChild(newOption);
    }
  },
  setAccount: function() {
      event.preventDefault();
      var self = this;
      if ($("#newAccountId").val()) {
        account = accounts[$("#newAccountId").val()]
      } else {
        account = accounts[0];
      }
      console.log("Account:"+account);

      self.setAccountLabel(account);
      self.setBalanceLabel(account);
  },
  setAccountLabel: function(account) {
    var status = document.getElementById("account");
    status.innerHTML = account;
  },
  setStatus: function(element,message) {
    var status = document.getElementById(element);
    status.innerHTML = message;
  },
  setBalanceLabel: function(address) {
    var self = this;

    return web3.eth.getBalancePromise(address)
    .then((value) =>{
      accountBalance = value;
      var balance = document.getElementById("balance");
      balance.innerHTML = value.valueOf();

      var vehicle_balance = document.getElementById("vehicle_balance");
      if(vehicle_balance) {
        vehicle_balance.innerHTML = value.valueOf();
      }

    }).catch(function(e) {
      console.log(e);
      self.setStatus("status","Error getting balance; see log.");
    });
  },
  promisifyWeb3: function(web3Instance) {
      Promise.promisifyAll(web3Instance.eth, { suffix: "Promise" });
      Promise.promisifyAll(web3Instance.version, { suffix: "Promise" });
      Promise.promisifyAll(web3Instance.net, { suffix: "Promise" });
      web3Instance.eth.getTransactionReceiptMined = require("../../utils/getTransactionReceiptMined.js");
      return web3Instance;
  },
  //
  // REGULATOR FUNCTIONS....
  //
  createNewOperator: function() {
    var self = this;
    event.preventDefault();
    $("#newOperatorButton").attr("disabled", true);

    var owner = $("#newOperatorOwner").val();
    var deposit = $("#newOperatorDeposit").val();

    if ((!isAddress(owner)) || (deposit <= 0)) {
      self.setStatus("newOperatorStatus","Owner need to be a valid address<br/>Deposit need to be a positive int");
      self.setStatus("newOperatorAddress","");
      $("#newOperatorButton").attr("disabled", false)
      return false;
    }

    return Regulator.deployed()
      .then((instance) => {
        regulatorInstance = instance;
        return regulatorInstance.getOwner();
      })
      .then((rowner) => {
        if (rowner != account) {
          document.getElementById("newOperatorOwner").classList.add("invalid");
          return false;
        } else {
          self.setStatus("newOperatorStatus","Creating operator...");
          return regulatorInstance.createNewOperator(owner,deposit,{from: account, gas: gas});
        }
      })
      .then((tx) => {
        $("#newOperatorButton").attr("disabled", false);
        if (tx) {
          var address = tx.logs[1].args.newOperator;
          var owner = tx.logs[1].args.owner;
          self.setStatus("newOperatorStatus","New Operator created at:");
          self.setStatus("newOperatorAddress",address);
          return TollBoothOperator.at(address).setPaused(false,{from: owner, gas: gas})
        }
        else
        {
          return false;
        }
      })
      .then((tx) => {
        console.log(tx);
      })
      .catch(function(error) {
          console.error(error);
      });
  },
  setVehicleType: function() {
    var self = this;
    event.preventDefault();
    $("#setVehicleTypeButton").attr("disabled", true);

    var vehicle = $("#setVehicleTypeVehicle").val();
    var type = $("#setVehicleTypeType").val();

    if ((!isAddress(vehicle)) || (type < 1)) {
      self.setStatus("setVehicleTypeStatus","Vehicle need to be a valid address<br/>Type need to be a positive int");
      $("#setVehicleTypeButton").attr("disabled", false)
      return false;
    }


    return Regulator.deployed()
      .then((instance) => {
        regulatorInstance = instance;
        return regulatorInstance.getOwner();
      })
      .then((rowner) => {
        if (rowner != account) {
          self.setStatus("setVehicleTypeStatus","You are not the regulator owner, try changing your account");
          $("#setVehicleTypeButton").attr("disabled", false);
          return false;
        } else {
          self.setStatus("setVehicleTypeStatus","Setting...");
          return regulatorInstance.setVehicleType(vehicle,type,{from: account, gas: gas});
        }
      })
      .then((tx) => {
        $("#setVehicleTypeButton").attr("disabled", false);
        if (tx) {
          self.setStatus("setVehicleTypeStatus",""+vehicle+":"+type);
        }
      })
      .catch(function(error) {
          console.error(error);
      });
  },
  //
  // OPERATOR FUNCTIONS
  //
  setTollBoothOperatorAddress: function() {
    var self = this;
    event.preventDefault();
    var op = $("#TollBoothOperatorAddress").val();
    if (!isAddress(op)) {
      self.setStatus("status","TollBoothOperator need to be a valid address");
      return false;
    }
    self.setStatus("status","Using TollBoothOperator at: <small>"+op+"</small>");
    operatorAddress = op;
  },
  addTollBooth: function() {
    var self = this;
    event.preventDefault();
    if (!isAddress(operatorAddress)) {
      self.setStatus("addTollBoothStatus","TollBoothOperator need to be a valid address");
      return false;
    }

    $("#addTollBoothButton").attr("disabled", true);
    var tollbooth = $("#addTollBoothAddress").val();

    if (!isAddress(tollbooth)) {
      self.setStatus("addTollBoothStatus","TollBooth need to be a valid address");
      $("#addTollBoothButton").attr("disabled", false);
      return false;
    }

    return TollBoothOperator.at(operatorAddress)
    .then((instance) => {
      operatorInstance = instance;
      return operatorInstance.getOwner();
    })
    .then((owner) => {
      if (account != owner) {
        self.setStatus("addTollBoothStatus","You are not the TollBoothOperator Owner, try changin your account");
        return false;
      }
      else
      {
        return operatorInstance.addTollBooth(tollbooth,{from: account, gas: gas});
      }
    })
    .then((tx) => {
      console.log(tx);
      $("#addTollBoothButton").attr("disabled", false);
      if(tx)
      {
        self.setStatus("addTollBoothStatus","TollBooth added");
      }
    })
    .catch(function(error) {
          console.error(error);
    });
  },
  setRoutePrice: function() {
    var self = this;
    event.preventDefault();
    if (!isAddress(operatorAddress)) {
      self.setStatus("status","TollBoothOperator need to be a valid address");
      return false;
    }

    $("#setRoutePriceButton").attr("disabled", true);
    var entryBooth = $("#setRoutePriceEntryBooth").val();
    var exitBooth = $("#setRoutePriceExitBooth").val();
    var priceWeis = $("#setRoutePricePrice").val();

    if((!isAddress(entryBooth)) || (!isAddress(exitBooth)) || (priceWeis <= 0)) {
      self.setStatus("setRoutePriceStatus","Entry and Exit Booths need to be a valid address and price need to be a positive int.");
      $("#setRoutePriceButton").attr("disabled", false);
      return false;
    }

    return TollBoothOperator.at(operatorAddress)
    .then((instance) =>{
      operatorInstance = instance;
      return operatorInstance.getOwner({from: account, gas: gas});
    })
    .then((owner) => {
      if (owner != account) {
        self.setStatus("setRoutePriceStatus","You are not the owner, please try to changing your account");
        return false;
      }
      else 
      {
        return operatorInstance.setRoutePrice(entryBooth, exitBooth, priceWeis,{from: account, gas: gas});
      }
    })
    .then((tx) => {
      console.log(tx);
      $("#setRoutePriceButton").attr("disabled", false);
      if (tx) {
        self.setStatus("setRoutePriceStatus","Price setted");
      }
    })
    .catch(function(error) {
          console.error(error);
    });
  },
  setMultiplier: function() {
    var self = this;
    event.preventDefault();
    if (!isAddress(operatorAddress)) {
      self.setStatus("status","TollBoothOperator need to be a valid address");
      return false;
    }

    $("#setMultiplierButton").attr("disabled", true);
    var vehicleType = $("#setMultiplierVehicleType").val();
    var multiplier = $("#setMultiplierMultiplier").val();

    if ((vehicleType <= 0) || (multiplier <= 0)) {
      self.setStatus("setMultiplierStatus","Vehicle Type and Multiplier need to be a positive int.");
      $("#setMultiplierButton").attr("disabled", false);
      return false;
    }

    return TollBoothOperator.at(operatorAddress)
    .then((instance) =>{
      operatorInstance = instance;
      return operatorInstance.getOwner({from: account, gas: gas});
    })
    .then((owner) => {
      if (owner != account) {
        self.setStatus("setMultiplierStatus","You are not the owner, please try to changing your account");
        return false;
      }
      else 
      {
        return operatorInstance.setMultiplier(vehicleType, multiplier,{from: account, gas: gas});
      }
    })
    .then((tx) => {
      console.log(tx);
      $("#setMultiplierButton").attr("disabled", false);
      if (tx) {
        self.setStatus("setMultiplierStatus","Multiplier setted");
      }
    })
    .catch(function(error) {
          console.error(error);
    });
  },
  //
  // VEHICLE FUNCTIONS
  //
  enterRoad: function() {
    var self = this;
    event.preventDefault();
    if (!isAddress(operatorAddress)) {
      self.setStatus("TollBoothOperatorStatus","TollBoothOperator need to be a valid address");
      return false;
    }

    $("#enterRoadButton").attr("disabled", true);
    var entryBooth = $("#enterRoadEntryBooth").val();
    var secret = $("#enterRoadSecret").val();
    var enterValue = $("#enterRoadValue").val() * 1;
    var hashSecret;

    console.log("account:"+account);
    console.log("entryBooth:"+entryBooth);
    console.log("enterValue:"+enterValue);


    var vehicleType=0;
    var multiplier=0;
    var basePrice=0;

    if ((!isAddress(entryBooth)) || (!secret) || (enterValue<1)|| (!enterValue)) {
      self.setStatus("enterRoadStatus","EntryBooth need to be an address and must provide a Secret number");
      $("#enterRoadButton").attr("disabled", false);
      return false;
    }

    if ((enterValue>vehicle_balance)) {
      self.setStatus("enterRoadStatus","You don't have enough balance.");
      $("#enterRoadButton").attr("disabled", false);
      return false;
    }
    const secret0 = toBytes32(secret);

    return TollBoothOperator.at(operatorAddress).then((instance) => {
      operatorInstance = instance;
      return operatorInstance.hashSecret(secret0,{from: account, gas: gas});
    })
    .then((hash) => {
      console.log("hash:"+hash);
      return operatorInstance.enterRoad(entryBooth,hash,{from: account, value: enterValue, gas: 10000000});
    })
    .then((tx) => {
      console.log(tx);
      $("#enterRoadButton").attr("disabled", false);
      if(tx) {
        self.setStatus("enterRoadStatus","You have entered the road");
      }
    })
    .catch(function(error) {
      console.error(error);
    });
    $("#enterRoadButton").attr("disabled", false);
  },
  getVehicleHistory: function(index) {
    var self = this;
    event.preventDefault();

    var historyLength = 0;
    var historyTable = $("#vehicleHistory");
    $("#vehicleHistoryEntries").empty();
    

    return Regulator.deployed()
      .then((instance) => {
        regulatorInstance = instance;
        return regulatorInstance.getVehicleHistoryLength.call(account);
      })
      .then((elementsCount) => {
        historyLength = elementsCount.toNumber();
        return self.getVehicleHistoryElement(historyLength -1);
      })
      .then((res) => {

      })
  },
  getVehicleHistoryElement: function(index) {
    var self = this;
    if(index >= 0) {
      var historyTable = $("#vehicleHistory");

      return regulatorInstance.getVehicleHistoryElement.call(account,index,{from: account, gas: gas})
        .then((data) =>{
          console.log(data);
          
          var newRow = $("<tr>");
          var cols = "";
          cols += '<td><small>'+data[0].toNumber()+'</small></td>';
          cols += '<td><small>'+data[1]+'</small></td>';
          cols += '<td><small>'+data[2]+'</small></td>';
          cols += '<td><small>'+data[3].toNumber()+'</small></td>';
          cols += '<td><small>'+data[4].toNumber()+'</small></td>';
          newRow.append(cols);
          historyTable.append(newRow);
          
          self.getVehicleHistoryElement(index - 1);
        })
    }
  },
  //
  // TOLLBOOTH FUNCTIONS
  //
  reportExitRoad: function() {
    var self = this;
    event.preventDefault();
    if (!isAddress(operatorAddress)) {
      self.setStatus("TollBoothOperatorStatus","TollBoothOperator need to be a valid address");
      return false;
    }

    $("#reportExitRoadButton").attr("disabled", true);
    var secret = $("#reportExitRoadSecret").val();

    if (!secret) {
      self.setStatus("reportExitRoadStatus","You must provide a secret");
      $("#reportExitRoadButton").attr("disabled", false);
      return false;
    }

    var secret = toBytes32(secret);

    return TollBoothOperator.at(operatorAddress)
    .then((instance) =>{
      operatorInstance = instance;
      console.log(instance);
      return operatorInstance.isTollBooth(account,{from: account, gas: gas});
    })
    .then((success) => {
      if (!success) {
        self.setStatus("reportExitRoadStatus","You are not a TollBooth, please try to changing your account");
        return false;
      }
      else 
      {
        return operatorInstance.reportExitRoad(secret, {from: account, gas: gas});
      }
    })
    .then((tx) => {
      $("#reportExitRoadButton").attr("disabled", false);
      if (tx) {
        console.log(tx);
        var status = "<div>";
        if (tx.logs[0].event == "LogPendingPayment") {
          status += "<b>Status:</b>Pending Payment<br/>";
        } else {
          status += "<b>Status:</b>Vehicle has left the road<br/>";
          status += "<b>Fee:</b>"+tx.logs[0].args.finalFee+" weis<br/>";
          status += "<b>Refunded:</b>"+tx.logs[0].args.refundWeis+" weis<br/>";
        }        
        status += "</div>";
        self.setStatus("reportExitRoadStatus",status);
      } else {
        self.setStatus("reportExitRoadStatus","unexpected error");
      }
    })
    .catch(function(error) {
          console.error(error);
    });
    $("#reportExitRoadButton").attr("disabled", false);
  },
};

window.addEventListener('load', function() {
  if (typeof web3 !== 'undefined') {
    //console.warn("Using web3 detected from external source. If you find that your accounts don't appear or you have 0 weis, ensure you've configured that source properly. If using MetaMask, see the following link. Feel free to delete this warning. :) http://truffleframework.com/tutorials/truffle-and-metamask")
    // Use Mist/MetaMask's provider
    window.web3 = new Web3(web3.currentProvider);
  } else {
    console.warn("No web3 detected. Falling back to http://localhost:8545. You should remove this fallback when you deploy live, as it's inherently insecure. Consider switching to Metamask for development. More info here: http://truffleframework.com/tutorials/truffle-and-metamask");
    // fallback - use your fallback strategy (local node / hosted node + in-dapp id mgmt / fail)
    window.web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
  }

  App.start();
});
