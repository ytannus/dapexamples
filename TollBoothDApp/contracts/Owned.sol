pragma solidity ^0.4.13;

import "./interfaces/OwnedI.sol";

contract Owned is OwnedI
{
	address owner;
	
	modifier fromOwner()
	{
		require(msg.sender == owner);
		_;
	}

	/*
	 * Constructor
	 */
	function Owned()
		public
	{
		owner = msg.sender;
	}

	/**
     * Sets the new owner for this contract.
     *     It should roll back if the caller is not the current owner.
     *     It should roll back if the argument is the current owner.
     *     It should roll back if the argument is a 0 address.
     * @param newOwner The new owner of the contract
     * @return Whether the action was successful.
     * Emits LogOwnerSet.
     */
	function setOwner(address newOwner)
		public
		fromOwner
		returns(bool success)
	{
		require(newOwner != 0x0);
		require(newOwner != owner);

		owner = newOwner;
		LogOwnerSet(msg.sender,newOwner);

		return(true);
	}

	/**
     * @return The owner of this contract.
     */
	function getOwner()
		constant
    	returns(address)
	{
		return(owner);
	}
}

