pragma solidity ^0.4.13;

import "./Owned.sol";
import "./interfaces/PausableI.sol";

/**
 * The Pausable contract rolls back the transaction if the contract is in the false paused state
 * or rolls back the transaction if the contract is in the true paused state
 */
contract Pausable is  Owned, PausableI
{

	bool paused;

	/*
	 * rolls back the transaction if the
     * contract is in the `false` paused state.
     */
	modifier whenPaused()
	{
		assert(isPaused());
		_;
	}

	/*
	 * rolls back the transaction if the
     * contract is in the `true` paused state.
     */
	modifier whenNotPaused()
	{
		assert(!isPaused());
		_;
	}

	/*
	 * Constructor
	 */
	function Pausable(bool _initialPausedState)
		public
	{
		paused = _initialPausedState;
//		LogPausedSet(msg.sender, _initialPausedState);
	}

	/**
     * Sets the new paused state for this contract.
     *     It should roll back if the caller is not the current owner of this contract.
     *     It should roll back if the state passed is no different from the current.
     * @param newState The new desired "paused" state of the contract.
     * @return Whether the action was successful.
     * Emits LogPausedSet.
     */
	function setPaused(bool newState)
		fromOwner
		returns(bool success)
	{
		require(newState != paused);
		paused = newState;
		LogPausedSet(msg.sender, newState);
		return(true);
	}

	/**
     * @return Whether the contract is indeed paused.
     */
	function isPaused()
		constant
		returns(bool isIndeed)
	{
		return(paused);
	}

}

