pragma solidity ^0.4.13;

import "./interfaces/RegulatedI.sol";
//import "./Regulator.sol";

/**
 * The Regulated contract
 */
contract Regulated is RegulatedI
{
	address currentRegulator;

    /*
     * Constructor
     */
	function Regulated(address _initialRegulator)
		public
	{
		require(_initialRegulator != 0x0);
		currentRegulator = _initialRegulator;

//		LogRegulatorSet(currentRegulator, _initialRegulator);
	}	
	/**
     * Sets the new regulator for this contract.
     *     It should roll back if any address other than the current regulator of this contract
     *       calls this function.
     *     It should roll back if the new regulator address is 0.
     *     It should roll back if the new regulator is the same as the current regulator.
     * @param newRegulator The new desired regulator of the contract.
     * @return Whether the action was successful.
     * Emits LogRegulatorSet.
     */
    function setRegulator(address newRegulator)
        public
        returns(bool success)
    {
        require(newRegulator != 0x0);
        require(currentRegulator == msg.sender);
    	require(currentRegulator != newRegulator);

    	currentRegulator = newRegulator;
    	LogRegulatorSet(msg.sender, newRegulator);
    	return(true);
    }
    /**
     * @return The current regulator.
     */
    function getRegulator()
        constant
        public
        returns(RegulatorI regulator)
    {
        regulator = RegulatorI(currentRegulator);
    }
}

