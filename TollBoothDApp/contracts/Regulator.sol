pragma solidity ^0.4.13;

import "./interfaces/RegulatorI.sol";
import "./TollBoothOperator.sol";
import "./Owned.sol";

/**
 * The Regulator
 */
contract Regulator is  Owned, RegulatorI
{

    /**
     * uint VehicleType:
     * 0: not a vehicle, absence of a vehicle
     * 1 and above: is a vehicle.
     * For instance:
     *   1: motorbike
     *   2: car
     *   3: lorry
     */
    mapping(address => uint) vehicles;

    mapping (address => bool) public registeredOperators;


    struct entryStruct {
        uint timestamp;
        address entryBooth;
        address exitBooth;
        uint price;
        uint refundWeis;
    }
    mapping (address => entryStruct[]) public vehicleHistory;
    

    /*
     * Constructor
     *
     * Sets owner
     */
    function Regulator()
        public
	{
	}

    /**
     * Called by the owner of the regulator to register a new vehicle with its VehicleType.
     *     It should roll back if the caller is not the owner of the contract.
     *     It should roll back if the arguments mean no change of state.
     *     It should roll back if a 0x vehicle address is passed.
     * @param vehicle The address of the vehicle being registered. This may be an externally
     *   owned account or a contract. The regulator does not care.
     * @param vehicleType The VehicleType of the vehicle being registered.
     *    passing 0 is equivalent to unregistering the vehicle.
     * @return Whether the action was successful.
     * Emits LogVehicleTypeSet
     */
	function setVehicleType(address vehicle, uint vehicleType)
        public
        fromOwner
        returns(bool success)
    {
        require(vehicles[vehicle] != vehicleType);
        require(vehicle != 0x0);

        vehicles[vehicle] = vehicleType;

        LogVehicleTypeSet(msg.sender, vehicle, vehicleType);
        return(true);
    }

    /**
     * @param vehicle The address of the registered vehicle.
     * @return The VehicleType of the vehicle whose address was passed. 0 means it is not
     *   a registered vehicle.
     */
    function getVehicleType(address vehicle)
        constant
        public
        returns(uint vehicleType)
    {
        return(vehicles[vehicle]);
    }

    /**
     * Called by the owner of the regulator to deploy a new TollBoothOperator onto the network.
     *     It should roll back if the caller is not the owner of the contract.
     *     It should start the TollBoothOperator in the `true` paused state.
     *     It should roll back if the rightful owner argument is the current owner of the regulator.
     * @param owner The rightful owner of the newly deployed TollBoothOperator.
     * @param deposit The initial value of the TollBoothOperator deposit.
     * @return The address of the newly deployed TollBoothOperator.
     * Emits LogTollBoothOperatorCreated.
     */
    function createNewOperator(
            address owner,
            uint deposit)
        public
        fromOwner
        returns(TollBoothOperatorI)
    {
        require (owner != msg.sender);
    
        TollBoothOperator newOperator = new TollBoothOperator(true,deposit,address(this));
        newOperator.setOwner(owner);//updates ownership of newOperator
        registeredOperators[newOperator] = true;
        LogTollBoothOperatorCreated(msg.sender, newOperator, owner, deposit);
        
        return(newOperator);
    }   

    /*
     * Called by the owner of the regulator to remove a previously deployed TollBoothOperator from
     * the list of approved operators.
     *     It should roll back if the caller is not the owner of the contract.
     *     It should roll back if the operator is unknown.
     * @param operator The address of the contract to remove.
     * @return Whether the action was successful.
     * Emits LogTollBoothOperatorRemoved.
     */
    function removeOperator(address operator)
        fromOwner
        public
        returns(bool success)
    {
        assert(registeredOperators[operator]);

        registeredOperators[operator] = false;
        LogTollBoothOperatorRemoved(msg.sender, operator);
        return(true);
    }

    /**
     * @param operator The address of the TollBoothOperator to test.
     * @return Whether the TollBoothOperator is indeed approved.
     */
    function isOperator(address operator)
        constant
        public
        returns(bool indeed)
    {
        return(registeredOperators[operator]);
    }

    /*
     *
     * Saves Vehicle exit history record
     *      Sender need to be a valid TollBoothOperator
     * @param vehicle Address
     * @param entryBooth address
     * @param exitBooth address
     * @param price payed by vehicle
     * @param refundWeis amount returned to vehicle when exits
     * @return result of saving the record 
     */
    function logExit(address vehicle, address entryBooth, address exitBooth, uint price, uint refundWeis)
        public
        returns(bool success)
    {
        entryStruct memory entry;
        entry.timestamp = now;
        entry.entryBooth = entryBooth;
        entry.exitBooth = exitBooth;
        entry.price = price;
        entry.refundWeis = refundWeis;
        vehicleHistory[vehicle].push(entry);
        return(true);
    }
    /*
     * Get vehicle history length
     */
    function getVehicleHistoryElement(address vehicle, uint index)
        public
        constant
        returns(uint timestamp,
            address entryBooth,
            address exitBooth,
            uint price,
            uint refundWeis)
    {
        timestamp = vehicleHistory[vehicle][index].timestamp;
        entryBooth = vehicleHistory[vehicle][index].entryBooth;
        exitBooth = vehicleHistory[vehicle][index].exitBooth;
        price = vehicleHistory[vehicle][index].price;
        refundWeis = vehicleHistory[vehicle][index].refundWeis;
    }
    
    /*
     * Return the lenth of the vehicle history 
     */
    function getVehicleHistoryLength(address vehicle)
        public
        constant
        returns(uint length)
    {
        length = vehicleHistory[vehicle].length;
    }
    
}
