pragma solidity ^0.4.13;

//import "./Owned.sol";
import "./TollBoothHolder.sol";
import "./interfaces/RoutePriceHolderI.sol";

/**
 * The RoutePriceHolder contract
 */
contract RoutePriceHolder is
    //Owned,        //inherits from TollBoothHolder
    TollBoothHolder,
    RoutePriceHolderI
{

    mapping (bytes32 => uint) routePrice; //route hash => price mapping

	function RoutePriceHolder()
		public
	{	
	}	

	 /**
     * Called by the owner of the RoutePriceHolder.
     *     It can be used to update the price of a route, including to zero.
     *     It should roll back if the caller is not the owner of the contract.
     *     It should roll back if one of the booths is not a registered booth.
     *     It should roll back if entry and exit booths are the same.
     *     It should roll back if either booth is a 0x address.
     *     It should roll back if there is no change in price.
     * @param entryBooth The address of the entry booth of the route set.
     * @param exitBooth The address of the exit booth of the route set.
     * @param priceWeis The price in weis of the new route.
     * @return Whether the action was successful.
     * Emits LogPriceSet.
     */
    function setRoutePrice(
            address entryBooth,
            address exitBooth,
            uint priceWeis)
        public
        fromOwner
        returns(bool success)
    {
        require(entryBooth != 0x0 && exitBooth != 0x0);
        require(entryBooth != exitBooth);
        require(isTollBooth(entryBooth) && isTollBooth(exitBooth));

        bytes32 hash = getRouteHash(entryBooth,exitBooth);
        require(routePrice[hash] != priceWeis);

        routePrice[hash] = priceWeis;

        LogRoutePriceSet(msg.sender, entryBooth, exitBooth, priceWeis);

        return(true);
    }

    /**
     * @param entryBooth The address of the entry booth of the route.
     * @param exitBooth The address of the exit booth of the route.
     * @return priceWeis The price in weis of the route.
     *     If the route is not known or if any address is not a booth it should return 0.
     *     If the route is invalid, it should return 0.
     */
    function getRoutePrice(
            address entryBooth,
            address exitBooth)
        constant
        public
        returns(uint priceWeis)
    {
        bytes32 hash = getRouteHash(entryBooth,exitBooth);
        return(routePrice[hash]);
    }

    /*
     * @param entryBooth The address of the entry booth of the route.
     * @param exitBooth The address of the exit booth of the route.
     * @return a uniq hash of a route
     */ 
    function getRouteHash(address entryBooth, address exitBooth)
        internal
        returns(bytes32)
    {
        return(keccak256(entryBooth,exitBooth));
    }
}

