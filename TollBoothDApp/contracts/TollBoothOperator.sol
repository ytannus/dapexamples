pragma solidity ^0.4.13;
import "./Regulated.sol";
import "./Pausable.sol";
import "./DepositHolder.sol";
import "./RoutePriceHolder.sol";
import "./MultiplierHolder.sol";

import "./interfaces/TollBoothOperatorI.sol";

import "./Regulator.sol";

/**
 * The TollBoothOperator contract
 */
contract TollBoothOperator is
//    Owned,
    Regulated,
    Pausable,           //inherits Owned
    DepositHolder,      //inherits Owned
    RoutePriceHolder,   //inherits Owned, TollBoothHolder
    MultiplierHolder,   //inherits Owned
    TollBoothOperatorI
{
    struct entryStruct {
        address vehicle;
        uint vehicleType;
        address entryBooth;
        address exitBooth;
        uint depositedAmount;
    }
    mapping (bytes32 => entryStruct) ticket; //exitSecretHashed => entryStruct

    struct pendingPaymentStruct {
        address entryBooth;
        address exitBooth;
        bytes32 exitSecretHashed;
    }
    
    pendingPaymentStruct[] public pendingPayments;
    
    uint collectedFeesAmount;
    
	/*
	 * Constructor
	 */
	function TollBoothOperator(
			bool _initialPausedState,
			uint _initialDeposit,
			address _initialRegulator)
        Pausable(_initialPausedState)
        DepositHolder(_initialDeposit)
        Regulated(msg.sender)
		public
	{
		require(_initialDeposit > 0);
		require(_initialRegulator != 0x0);
	}

	/**
     * This provides a single source of truth for the encoding algorithm.
     * @param secret The secret to be hashed.
     * @return the hashed secret.
     */
    function hashSecret(bytes32 secret)
        constant
        public
        returns(bytes32 hashed)
    {
    	return(keccak256(secret));
    }

    /**
     * Called by the vehicle entering a road system.
     * Off-chain, the entry toll booth will open its gate up successful deposit and confirmation
     * of the vehicle identity.
     *     It should roll back when the contract is in the `true` paused state.
     *     It should roll back if `entryBooth` is not a tollBooth.
     *     It should roll back if less than deposit * multiplier was sent alongside.
     *     It should be possible for a vehicle to enter "again" before it has exited from the 
     *       previous entry.
     * @param entryBooth The declared entry booth by which the vehicle will enter the system.
     * @param exitSecretHashed A hashed secret that when solved allows the operator to pay itself.
     *   A previously used exitSecretHashed cannot be used ever again.
     * @return Whether the action was successful.
     * Emits LogRoadEntered.
     */
    function enterRoad(
            address entryBooth,
            bytes32 exitSecretHashed)
        public
        whenNotPaused
        payable
        returns (bool success)
    {
        assert(isTollBooth(entryBooth));

        require(ticket[exitSecretHashed].exitBooth == 0x0); //if != 0x0 exitSecretHashed was used
        
        address reg = getRegulator();
        uint vehicleType = Regulator(reg).getVehicleType(msg.sender);
        assert(msg.value >= getMultiplier(vehicleType) * getDeposit());

        ticket[exitSecretHashed].vehicle = msg.sender;
        ticket[exitSecretHashed].entryBooth = entryBooth;
        ticket[exitSecretHashed].depositedAmount = msg.value;
        ticket[exitSecretHashed].vehicleType = vehicleType;

    	LogRoadEntered(msg.sender,entryBooth,exitSecretHashed,msg.value);
    	return(true);
    }

    /**
     * @param exitSecretHashed The hashed secret used by the vehicle when entering the road.
     * @return The information pertaining to the entry of the vehicle.
     *     vehicle: the address of the vehicle that entered the system.
     *     entryBooth: the address of the booth the vehicle entered at.
     *     depositedWeis: how much the vehicle deposited when entering.
     * After the vehicle has exited, `depositedWeis` should be returned as `0`.
     * If no vehicles had ever entered with this hash, all values should be returned as `0`.
     */
    function getVehicleEntry(bytes32 exitSecretHashed)
        constant
        public
        returns(
            address vehicle,
            address entryBooth,
            uint depositedWeis)
    {
    	vehicle        = ticket[exitSecretHashed].vehicle;
    	entryBooth     = ticket[exitSecretHashed].entryBooth;
    	depositedWeis  = ticket[exitSecretHashed].depositedAmount;
    }

    /**
     * Called by the exit booth.
     *     It should roll back when the contract is in the `true` paused state.
     *     It should roll back when the sender is not a toll booth.
     *     It should roll back if the exit is same as the entry.
     *     It should roll back if the secret does not match a hashed one.
     * @param exitSecretClear The secret given by the vehicle as it passed by the exit booth.
     * @return status:
     *   1: success, -> emits LogRoadExited
     *   2: pending oracle -> emits LogPendingPayment
     */
    function reportExitRoad(bytes32 exitSecretClear)
        whenNotPaused
        public
        returns (uint status)
    {
        bytes32 exitSecretHashed = hashSecret(exitSecretClear);

        require(isTollBooth(msg.sender));
        require(msg.sender != ticket[exitSecretHashed].entryBooth);
        require(ticket[exitSecretHashed].vehicle != 0x0); //ensure exist entry

        ticket[exitSecretHashed].exitBooth = msg.sender;

        uint routePrice = getRoutePrice(ticket[exitSecretHashed].entryBooth, msg.sender);
        uint finalFee = getMultiplier(ticket[exitSecretHashed].vehicleType) * routePrice;

        if (finalFee > 0) //route price exists
        {
            status = 1;
            uint refundWeis = executePayment(exitSecretHashed, finalFee);
            LogRoadExited(ticket[exitSecretHashed].exitBooth,exitSecretHashed,finalFee,refundWeis);
            Regulator(getRegulator()).logExit(ticket[exitSecretHashed].vehicle,ticket[exitSecretHashed].entryBooth,ticket[exitSecretHashed].exitBooth,finalFee,refundWeis);
        }
        else //pending payment, routePrice == 0
        {
            status = 2;

            pendingPaymentStruct memory pendingPayment;
            pendingPayment.entryBooth = ticket[exitSecretHashed].entryBooth;
            pendingPayment.exitBooth = ticket[exitSecretHashed].exitBooth;
            pendingPayment.exitSecretHashed = exitSecretHashed;
            pendingPayments.push(pendingPayment);

            LogPendingPayment(exitSecretHashed,ticket[exitSecretHashed].entryBooth,ticket[exitSecretHashed].exitBooth);
        }
    }

    /**
     * @param entryBooth the entry booth that has pending payments.
     * @param exitBooth the exit booth that has pending payments.
     * @return the number of payments that are pending because the price for the
     * entry-exit pair was unknown.
     */
    function getPendingPaymentCount(address entryBooth, address exitBooth)
        constant
        public
        returns (uint count)
    {
        //count = 0;
        for (uint i=0;i<pendingPayments.length;i++)
        {
            if(pendingPayments[i].entryBooth == entryBooth && pendingPayments[i].exitBooth == exitBooth)
            {
                count++;
            }
        }
    }

    /*
     * Overwrites RoutePriceHolder setRoutePrice to clear 1 pending payment when
     * a new route price is set
     *
     * @param entryBooth The address of the entry booth of the route set.
     * @param exitBooth The address of the exit booth of the route set.
     * @param priceWeis The price in weis of the new route.
     * @return Whether the action was successful.
     * Emits LogPriceSet and LogRoadExited (if exist a pending payment for this route)
     */
    
    function setRoutePrice(
            address entryBooth,
            address exitBooth,
            uint priceWeis)
        public
        fromOwner
        returns(bool success)
    {
        RoutePriceHolder.setRoutePrice(entryBooth,exitBooth,priceWeis);
        //Clears 1 Pending Payment if exists
        if(pendingPayments.length>0)
        {
            clearSomePendingPayments(entryBooth,exitBooth,1);
        }
        return(true);
    }
    
    /**
     * Can be called by anyone. In case more than 1 payment was pending when the oracle gave a price.
     *     It should roll back when the contract is in `true` paused state.
     *     It should roll back if booths are not really booths.
     *     It should roll back if there are fewer than `count` pending payment that are solvable.
     *     It should roll back if `count` is `0`.
     * @param entryBooth the entry booth that has pending payments.
     * @param exitBooth the exit booth that has pending payments.
     * @param count the number of pending payments to clear for the exit booth.
     * @return Whether the action was successful.
     * Emits LogRoadExited as many times as count.
     */
    
    function clearSomePendingPayments(
            address entryBooth,
            address exitBooth,
            uint count)
        whenNotPaused
        public
        returns (bool success)
    {
        require(isTollBooth(entryBooth) && isTollBooth(exitBooth)); 

        if (pendingPayments.length < count || count == 0) {
            return(true);
        }

        for (uint i=0;i<pendingPayments.length && count > 0;i++)
        {
            
            if(pendingPayments[i].entryBooth == entryBooth && pendingPayments[i].exitBooth == exitBooth)
            {
                count--;
                bytes32 exitSecretHashed = pendingPayments[i].exitSecretHashed;
                uint finalFee =0;
                uint refundWeis =0;
                uint routePrice = getRoutePrice(entryBooth, exitBooth);

                uint vehicleType = ticket[exitSecretHashed].vehicleType;

                finalFee = getMultiplier(vehicleType) * routePrice;
                
                if (ticket[exitSecretHashed].depositedAmount < finalFee)
                {
                    finalFee = ticket[exitSecretHashed].depositedAmount;
                }
                refundWeis = executePayment(exitSecretHashed,finalFee);
                delete pendingPayments[i];

                LogRoadExited(ticket[exitSecretHashed].exitBooth,exitSecretHashed,finalFee,refundWeis);
                Regulator(getRegulator()).logExit(ticket[exitSecretHashed].vehicle,ticket[exitSecretHashed].entryBooth,ticket[exitSecretHashed].exitBooth,finalFee,refundWeis);
            }
        }
        
    	return(true);
        
    }
    

    /*
     * @param exitSecretHashed The hashed secret used by the vehicle when entering the road.
     * @return the amount to be refund to vehicle
     * Emits LogRoadExited
     */
    function executePayment(bytes32 exitSecretHashed, uint finalFee)
        internal
        returns(uint refundWeis)
    {
        refundWeis = 0;
        if(ticket[exitSecretHashed].depositedAmount > finalFee)
        {
            refundWeis = ticket[exitSecretHashed].depositedAmount - finalFee;
            ticket[exitSecretHashed].vehicle.transfer(refundWeis);
        }
       
        ticket[exitSecretHashed].depositedAmount = 0;
        collectedFeesAmount += finalFee;
    }

    /**
     * @return The amount that has been collected through successful payments. This is the current
     *   amount, it does not reflect historical fees. So this value goes back to zero after a call
     *   to `withdrawCollectedFees`.
     */
    function getCollectedFeesAmount()
        constant
        public
        returns(uint amount)
    {
    	return(collectedFeesAmount);
    }

    /**
     * Called by the owner of the contract to withdraw all collected fees (not deposits) to date.
     *     It should roll back if any other address is calling this function.
     *     It should roll back if there is no fee to collect.
     *     It should roll back if the transfer failed.
     * @return success Whether the operation was successful.
     * Emits LogFeesCollected.
     */
    function withdrawCollectedFees()
        fromOwner
        public
        returns(bool success)
    {
        assert(collectedFeesAmount > 0);

        uint amount = collectedFeesAmount;
        collectedFeesAmount = 0;

        msg.sender.transfer(amount);

        LogFeesCollected(msg.sender,amount);
    	return(true);
    }


    /*
     * fallback
     */

	function()
		public
	{
		revert();
	}
}

