var Regulator = artifacts.require("./Regulator.sol");
var TollBoothOperator = artifacts.require("./TollBoothOperator.sol");

//Deploy Regulator and create a new TollBoothOperator
//All new operator are created paused

module.exports = function(deployer) {

  var regulatorInstance;
  var operatorInstance;
  var account0 = web3.eth.accounts[0];
  var account1 = web3.eth.accounts[1];

  deployer.deploy(Regulator, {gas: 10000000}).then(function() {
  	return Regulator.deployed()
  }).then((regulator) => {
  	regulatorInstance = regulator;
  	return regulatorInstance.createNewOperator(account1,11,{from: account0})
  }).then((tx) => {
  	console.log("===> Deployed Regulator:"+regulatorInstance.address)
  	return(tx.logs[1].args.newOperator)
  }).then((operator) => {
  	console.log("===> New Operator:"+operator)
  	operatorInstance = TollBoothOperator.at(operator)
  	return operatorInstance.setPaused(false,{from: account1}) //Resumes operator
  }).then((tx)=>{
  	console.log("===> Resumes Operator:"+tx.logs[0].event+"(false)")
  })
};
