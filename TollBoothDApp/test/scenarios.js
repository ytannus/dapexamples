Promise = require("bluebird");
Promise.allNamed = require("../utils/sequentialPromiseNamed.js");
const randomIntIn = require("../utils/randomIntIn.js");
const toBytes32 = require("../utils/toBytes32.js");

if (typeof web3.eth.getAccountsPromise === "undefined") {
    Promise.promisifyAll(web3.eth, { suffix: "Promise" });
}

const Regulator = artifacts.require("./Regulator.sol");
const TollBoothOperator = artifacts.require("./TollBoothOperator.sol");

contract('TollBoothOperator', function(accounts) {
	let owner0, owner1,
        booth0, booth1, booth2,
        vehicle0, vehicle1,
        regulator, operator;
    const price01 = randomIntIn(1, 1000);
    const deposit0 = price01 + randomIntIn(1, 1000);
    const deposit1 = deposit0 + randomIntIn(1, 1000);
    const vehicleType0 = randomIntIn(1, 1000);
    const vehicleType1 = vehicleType0 + randomIntIn(1, 1000);
    const multiplier0 = randomIntIn(1, 1000);
    const multiplier1 = multiplier0 + randomIntIn(1, 1000);
    const tmpSecret = randomIntIn(1, 1000);
    const secret0 = toBytes32(tmpSecret);
    const secret1 = toBytes32(tmpSecret + randomIntIn(1, 1000));
    const secret2 = toBytes32(tmpSecret + randomIntIn(1001, 2000));
    const secret3 = toBytes32(tmpSecret + randomIntIn(2001, 3000));
    const secret4 = toBytes32(tmpSecret + randomIntIn(3001, 4000));
    const secret5 = toBytes32(tmpSecret + randomIntIn(4001, 5000));
    const secret6 = toBytes32(tmpSecret + randomIntIn(5001, 6000));
    const extraAmount0 = randomIntIn(1, 1000);
    const extraAmount1 = extraAmount0 + randomIntIn(1, 1000);
    let hashed0, hashed1, hashed2, hashed3, hashed4, hashed5, hashed6;

    before("should prepare", function() {
        assert.isAtLeast(accounts.length, 8);
        owner0 = accounts[0];
        owner1 = accounts[1];
        booth0 = accounts[2];
        booth1 = accounts[3];
        booth2 = accounts[4];
        vehicle0 = accounts[5];
        vehicle1 = accounts[6];
        return web3.eth.getBalancePromise(owner0)
            .then(balance => assert.isAtLeast(web3.fromWei(balance).toNumber(), 10));
    });

    describe("Scenarios", function() {
    	
    	before("gets deployed regulator and creates a new operator", function() {
            return Regulator.deployed({ from: owner0 })
                .then(instance => regulator = instance)
                .then(() => regulator.setVehicleType(vehicle0, vehicleType0, { from: owner0 }))
                .then(tx => regulator.setVehicleType(vehicle1, vehicleType1, { from: owner0 }))
                .then(tx => regulator.createNewOperator(owner1, deposit0, { from: owner0 }))
                .then(tx => operator = TollBoothOperator.at(tx.logs[1].args.newOperator))
                .then(() => operator.addTollBooth(booth0, { from: owner1 }))
                .then(tx => operator.addTollBooth(booth1, { from: owner1 }))
                .then(tx => operator.addTollBooth(booth2, { from: owner1 }))
                .then(tx => operator.setMultiplier(vehicleType0, multiplier0, { from: owner1 }))
                .then(tx => operator.setMultiplier(vehicleType1, multiplier1, { from: owner1 }))
                .then(tx => operator.setRoutePrice(booth0, booth1, price01, { from: owner1 }))
                .then(tx => operator.setPaused(false, { from: owner1 }))
                .then(tx => operator.hashSecret(secret0))
                .then(hash => hashed0 = hash)
                .then(tx => operator.hashSecret(secret1))
                .then(hash => hashed1 = hash)
                .then(tx => operator.hashSecret(secret2))
                .then(hash => hashed2 = hash)
                .then(tx => operator.hashSecret(secret3))
                .then(hash => hashed3 = hash)
                .then(tx => operator.hashSecret(secret4))
                .then(hash => hashed4 = hash)
                .then(tx => operator.hashSecret(secret5))
                .then(hash => hashed5 = hash)
                .then(tx => operator.hashSecret(secret6))
                .then(hash => hashed6 = hash)
        });
    	

        it("Scenario 1: Route price is exact deposited amount, no refund", function() {
        	//Sets route price
        	return operator.setRoutePrice(booth0, booth1, deposit0, { from: owner1 })
        	.then((tx) => {
        		assert.strictEqual(tx.logs[0].event, "LogRoutePriceSet");
        		return operator.enterRoad(booth0, hashed0, { from: vehicle0, value: deposit0 * multiplier0 })
        	})
        	.then((tx) => {
        		const logEntered = tx.logs[0];
        		assert.strictEqual(logEntered.event, "LogRoadEntered");
        		assert.strictEqual(logEntered.args.depositedWeis.toNumber(), (deposit0 * multiplier0));
        		return operator.reportExitRoad(secret0, { from: booth1 })
        	})
        	.then((tx) => {
        		const logExited = tx.logs[0];
        		assert.strictEqual(logExited.event, "LogRoadExited");
        		assert.strictEqual(logExited.args.finalFee.toNumber(), deposit0 * multiplier0);
                assert.strictEqual(logExited.args.refundWeis.toNumber(), 0);
        	});	
        });


        it("Scenario 2: Route price is more than deposited amount, no refund", function() {
        	//Sets route price
        	return operator.setRoutePrice(booth0, booth1, deposit0 + extraAmount0, { from: owner1 })
        	.then((tx) => {
        		assert.strictEqual(tx.logs[0].event, "LogRoutePriceSet");
        		return operator.enterRoad(booth0, hashed1, { from: vehicle0, value: deposit0* multiplier0 })
        	})
        	.then((tx) => {
        		const logEntered = tx.logs[0];
        		assert.strictEqual(logEntered.event, "LogRoadEntered");
        		assert.strictEqual(logEntered.args.depositedWeis.toNumber(), (deposit0 * multiplier0));
        		return operator.reportExitRoad(secret1, { from: booth1 })
        	})
        	.then((tx) => {
        		const logExited = tx.logs[0];
        		assert.strictEqual(logExited.event, "LogRoadExited");
        		assert.strictEqual(logExited.args.finalFee.toNumber(), (deposit0 + extraAmount0) * multiplier0);
                assert.strictEqual(logExited.args.refundWeis.toNumber(), 0);
        	});	
        });


        it("Scenario 3: Route price is less than deposited amount, refunds", function() {

        	return operator.setDeposit(price01 + extraAmount0, { from: owner1 })
        	.then((tx) => {
        		assert.strictEqual(tx.logs[0].event, "LogDepositSet");
        	    return operator.setRoutePrice(booth0, booth1, price01, { from: owner1 })
        	})
        	.then((tx) => {
        		assert.strictEqual(tx.logs[0].event, "LogRoutePriceSet");
        		return operator.enterRoad(booth0, hashed2, { from: vehicle0, value: (price01 + extraAmount1) * multiplier0 })
        	})
        	.then((tx) => {
        		const logEntered = tx.logs[0];
        		assert.strictEqual(logEntered.event, "LogRoadEntered");
        		assert.strictEqual(logEntered.args.depositedWeis.toNumber(), (price01 + extraAmount1) * multiplier0);
        		return operator.reportExitRoad(secret2, { from: booth1 })
        	})
        	.then((tx) => {
        		const logExited = tx.logs[0];
        		assert.strictEqual(logExited.event, "LogRoadExited");
        		assert.strictEqual(logExited.args.finalFee.toNumber(), price01 * multiplier0);
                assert.strictEqual(logExited.args.refundWeis.toNumber(), extraAmount1 * multiplier0);
        	});	
        });


        it("Scenario 4: Vehicle deposits more than Base price and Route Price is equal to base Price, refunds", function() {
        	//Sets base price
        	return operator.setDeposit(deposit1, { from: owner1 })
        	.then((tx) => {
        		assert.strictEqual(tx.logs[0].event, "LogDepositSet");
        		return operator.setRoutePrice(booth0, booth1, deposit1, { from: owner1 })
        	})
        	.then((tx) => {
        		assert.strictEqual(tx.logs[0].event, "LogRoutePriceSet");
        		return operator.enterRoad(booth0, hashed3, { from: vehicle0, value: (deposit1 + extraAmount0) * multiplier0 })
        	})
        	.then((tx) => {
        		const logEntered = tx.logs[0];
        		assert.strictEqual(logEntered.event, "LogRoadEntered");
        		assert.strictEqual(logEntered.args.depositedWeis.toNumber(), (deposit1 + extraAmount0) * multiplier0);
        		return operator.reportExitRoad(secret3, { from: booth1 })
        	})
        	.then((tx) => {
        		const logExited = tx.logs[0];
        		assert.strictEqual(logExited.event, "LogRoadExited");
        		assert.strictEqual(logExited.args.finalFee.toNumber(), deposit1 * multiplier0);
                assert.strictEqual(logExited.args.refundWeis.toNumber(), extraAmount0 * multiplier0);
        	});	
        });

        it("Scenario 5: Vehicle deposits more than Base Price, Route price is unknown. Refunds after route price is sets and is lower than deposited amount ", function() {
        	//extraAmount0 < extraAmount1
        	//RoutePrice booth0--->booth2 is unknown
        	return operator.setDeposit(deposit0 + extraAmount0, { from: owner1 })
        	.then((tx) => {
        		assert.strictEqual(tx.logs[0].event, "LogDepositSet");
        		return operator.enterRoad(booth0, hashed4, { from: vehicle0, value: (deposit0 + extraAmount1) * multiplier0 })
        	})
        	.then((tx) => {
        		const logEntered = tx.logs[0];
        		assert.strictEqual(logEntered.event, "LogRoadEntered");
        		assert.strictEqual(logEntered.args.depositedWeis.toNumber(), (deposit0 + extraAmount1) * multiplier0);
        		return operator.reportExitRoad(secret4, { from: booth2 }) //Unknown route price
        	})
        	.then((tx) => {
        		const logPending = tx.logs[0];
        		assert.strictEqual(logPending.event, "LogPendingPayment");
        		assert.strictEqual(logPending.args.exitSecretHashed, hashed4);
        		assert.strictEqual(logPending.args.entryBooth, booth0);
        		assert.strictEqual(logPending.args.exitBooth, booth2);
                return operator.setRoutePrice(booth0,booth2, deposit0,{from: owner1})
        	})
        	.then((tx) => {
        		const logExited = tx.logs[1];
        		assert.strictEqual(tx.logs[0].event, "LogRoutePriceSet");
        		assert.strictEqual(logExited.event, "LogRoadExited");
        		assert.strictEqual(logExited.args.finalFee.toNumber(), deposit0 * multiplier0);
                assert.strictEqual(logExited.args.refundWeis.toNumber(), extraAmount1 * multiplier0);
        	});
        });

        it("Scenario 6: RoutePrice is unknown, 2 vehicles passed, after the price is established refunds to them", function() {
        	//extraAmount0 < extraAmount1
        	//RoutePrice booth1--->booth2 is unknown
        	//from Scenario 5: Deposit = deposit0 + extraAmount0
            
        	return operator.enterRoad(booth1, hashed5, { from: vehicle0, value: (deposit0 + extraAmount1) * multiplier0 })
        	.then((tx) => {
        		const logEntered = tx.logs[0];
        		assert.strictEqual(logEntered.event, "LogRoadEntered");
        		assert.strictEqual(logEntered.args.depositedWeis.toNumber(), (deposit0 + extraAmount1) * multiplier0);
        		return operator.reportExitRoad(secret5, { from: booth2 }) //Unknown route price
        	})
        	.then((tx) => {
        		const logPending = tx.logs[0];
        		assert.strictEqual(logPending.event, "LogPendingPayment");
        		assert.strictEqual(logPending.args.exitSecretHashed, hashed5);
        		assert.strictEqual(logPending.args.entryBooth, booth1);
        		assert.strictEqual(logPending.args.exitBooth, booth2);
        		return operator.enterRoad(booth1, hashed6, { from: vehicle1, value: (deposit0 + extraAmount0) * multiplier1 })
        	})
        	.then((tx) => {
        		const logEntered = tx.logs[0];
        		assert.strictEqual(logEntered.event, "LogRoadEntered");
        		assert.strictEqual(logEntered.args.depositedWeis.toNumber(), (deposit0 + extraAmount0) * multiplier1);
        		return operator.reportExitRoad(secret6, { from: booth2 }) //Unknown route price
        	})
        	.then((tx) => {
        		const logPending = tx.logs[0];
        		assert.strictEqual(logPending.event, "LogPendingPayment");
        		assert.strictEqual(logPending.args.exitSecretHashed, hashed6);
        		assert.strictEqual(logPending.args.entryBooth, booth1);
        		assert.strictEqual(logPending.args.exitBooth, booth2);
        		return operator.setRoutePrice(booth1,booth2, deposit0 ,{from: owner1})
        	})
        	.then((tx) => {
        		assert.strictEqual(tx.logs[0].event, "LogRoutePriceSet");
        		const logExited = tx.logs[1];
        		assert.strictEqual(logExited.event, "LogRoadExited");
        		assert.strictEqual(logExited.args.finalFee.toNumber(), deposit0 * multiplier0);
                assert.strictEqual(logExited.args.refundWeis.toNumber(), extraAmount1 * multiplier0);
                return operator.clearSomePendingPayments(booth1,booth2,1, { from: owner0 });
        	})
        	.then((tx) => {
        		const logExited = tx.logs[0];
        		assert.strictEqual(logExited.event, "LogRoadExited");
        		assert.strictEqual(logExited.args.finalFee.toNumber(), deposit0 * multiplier1);
                assert.strictEqual(logExited.args.refundWeis.toNumber(), extraAmount0 * multiplier1);
        	});
        });
	});
});