const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
  entry: {
    app: './app/javascripts/app.js',
    regulator: './app/javascripts/regulator.js',
    operator: './app/javascripts/operator.js',
    vehicle: './app/javascripts/vehicle.js',
    tollbooth: './app/javascripts/tollbooth.js'
  },
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: "[name].js"
  },
  plugins: [
    // Copy our app's index.html to the build folder.
    new CopyWebpackPlugin([
      { from: './app/index.html', to: "index.html" },
    ]),
    new CopyWebpackPlugin([
      { from: './app/pages', to: "pages" },
      { from: './app/stylesheets', to: "css" },
      { from: './app/javascripts', to: "js" },
      { from: './app/vendorjs', to: "vjs" }
    ]),
  ],
  module: {
    rules: [
      {
       test: /\.css$/,
       use: [ 'style-loader', 'css-loader' ]
      }
    ],
    loaders: [
      { test: /\.json$/, use: 'json-loader' },
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel-loader',
        query: {
          presets: ['es2015'],
          plugins: ['transform-runtime']
        }
      }
    ]
  },
  devServer: {
    stats: "errors-only",
    host: '0.0.0.0',
    port: 8000,
    open: true,
    overlay: true,
    //publicPath: "/", // here's the change
    //contentBase: path.join(__dirname, 'app'),
    watchOptions: {
      // Delay the rebuild after the first change
      aggregateTimeout: 300,
      // Poll using interval (in ms, accepts boolean too)
      poll: 1000,
      ignored: /node_modules/
    },
  }
}
