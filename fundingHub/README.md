# FundingHub

B9Lab final exam project.

This is a Funding Hub developed based on Truffle framework with react-box.
This will deploy a FundingHub and a example Project to start using it.

---

### Prerequisites
You should have a full working ethereum node to test this Dapp and meet all following prerequisites:

* geth v1.6.7 and/or ethereum-testrpc v3.0.3
* node v6.11.2
* npm v3.10.10
* truffle v3.4.8
* solc v0.4.10

Once you have installed all prerequisites also you need to:
* start the node (geth or testrpc)
* start miner (if you are using geth)
* unlock your account to deploy contracts and for the web interface contribution and refund actions.

## INSTALL Dapp

* Create a dedicated folder to store the code, for example FundingHub.
* Clone the project repository
* Install missing nodejs (via npm) dependencies
* Compile contracts
* Migrate compiled contracts to running node (use --reset flag to ensure all will be deployed)

```
mkdir FundingHub
cd FundingHub
git clone git@git.academy.b9lab.com:KBRNM-DEV-1-exam-projects/ytannus.git .
npm install --missing --save
truffle compile
truffle migrate --reset
```

Migration command will create one project as example with:
* Goal: 10 [ether]
* Deadline: <1 year from now>

---

### Running the tests

This project includes refund automatic test.
```
truffle test
```

And you should get:
```
  Contract: FundingHub
    Testing project creation
      ✓ Create Project (45ms)
      ✓ Check if project is stored in the Hub (48ms)
      ✓ Check new project initial values (62ms)
    Testing contribute
      ✓ Check contribute 3 ether (61ms)
      ✓ Check project balance (3 ethers)
    Testing refund
      ✓ Refund 1 ether (56ms)
      ✓ Withdraw (refund 1 ether) (53ms)
      ✓ Check project balance (2 ethers)
      ✓ Check project raised value (2 ethers) (49ms)
    Testing payout
      ✓ Check payout() (48ms)
      ✓ Withdraw (payout) (50ms)
      ✓ Check project balance (0 ethers)
      ✓ Check project raised value (2 ethers) (47ms)
      ✓ Check project closed (46ms)
```

To run test you need to install bluebird libs:
```
npm install bluebird--save
npm install bluebird-retry --save
```

> Don't forget to restart node after run tests

---

## USAGE

Ensure that geth or testrpc node is run and you account is unlocked

```
npm run start
```
This will open a browser pointing to:
http://localhost:3000

You should get the landing following page including one project created at migration step (created during the installation)

![FundingHub](public/1.png "Landing")

You can check if deploy is right if you got:
* FundingHub instance address (created at migration step)
* You account address
* Ensure that you have enough balance to operate with the Dapp
* Ensure that you have your account unblocked

![FundingHub](public/2.png "Hub Information")


### Create new project

To create a new project you need to provide:
* Goal amount to be reached expressed in Ethers
* Deadline date greater than current date

![FundingHub](public/3.png "New project Form")

New projects will be showen at right panel under "Active Projects" section:

![FundingHub](public/4.png "New project")

### Contribute or Refund

Each project has its own action form to perform contributions or refunds.
You should the amount of contribution or refund expressed in Weis (10^-18: ether) and selection de desired action:

![FundingHub](public/5.png "Contribute/Refund project")

Dapp will check:
* check if you have enough balance
* check if the project has enough balance raised (for refunds)
* Execute the transaction and
* Update your and project balances

![FundingHub](public/6.png "Project card updated")

---

## Author

* **Yery Tannus** - <ytannus@itaum.com>

## ToDo
* Improve UI

## Acknowledgments

* B9Lab
* IT Bar
