fbc#1: You make it possible to refund at any time, even after the goal was reached, which is akin to sabotage.
fbc#2: Moreover, because you forgot to update the state after a refund, I can refund many times, so in effect, making it possible to steal from other contributors. This could even be done all in one transaction within an attack contract. This means that, in 1 well-calibrated transaction, the contract would go from x Ethers to zero. There would be no time for others to rush for the exit because the contract would already be empty.
fbc#3: Account for the fact that a funding is considered made when you pass a simple parameter instead of Ether. So it is possible to pretend to have sent something then ask for a refund without even sending Ether in the first place.
fbc#4: Because you update the project status before you account for the new funding, you do not detect that the goal was reached at the time it was reached, but at the next funding transaction, which, necessarily will have to take the actual funding above the goal. It means that there will be situations where it is not possible to payout even when you have reached the goal.
fbc#5: It is weird how you make it possible to start a project with a non-zero contributed although no Ether would have been sent at construction.
fbc#6: Only the owner can make a contribution, which defeats the purpose of raising money.
fbc#7: Moreover, when it is deployed by the Hub, the hub is the owner, so it can fund the project in its own name with other people's money.
fbc#8: So in this case, only the hub can refund or payout, but there are no functions to achieve that, so the Ethers, if any, will be stuck in the Project contract.
fbc#9: The fallback function is made payable but there is no accounting of it, so no way to collect it back unless you use the stealing vulnerability.
fbc#10: Owner and deadline are saved in 2 different places, this is a waste of gas.
fbc#11: The contribute function on Hub does a duplicate check that is also done on Project, wasting gas.
fbc#12: It should have been possible to payout as soon as the goal is reached, and not need to wait for the end of the deadline.
