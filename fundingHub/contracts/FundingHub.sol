pragma solidity ^0.4.11;
import "./Project.sol";
contract FundingHub {
  mapping (address => uint) flags; //To ensure that project is created using this Hub instance.
  address[] projects; //Tracks created Projects
  event LogCreateProject(address _owner, address _projectAddress, uint _goal, uint _deadline);
  event LogContributionMade(address _contributor, address _projectAddress, uint _amount, uint balance);
  //createproject()
  // This function should allow a user to add a new project to the FundingHub.
  // The function should deploy a new Project contract and keep track of its
  // address. The createProject() function should accept all constructor values
  // that the Project contract requires.
  function createProject(uint _goal, uint _deadline) returns (address) {
    //require(_deadline > now && _goal > 0); //commented because FB3.2
    Project newProject = new Project(msg.sender, _goal,_deadline);
    flags[newProject] = 1; //Project created via this Hub
    projects.push(newProject);
    LogCreateProject(msg.sender, newProject, _goal, _deadline);
    return newProject;
  }
  // contribute()
  // This function allows users to contribute to a Project identified by its address.
  // contribute calls the fund() function in the individual Project contract
  // and passes on all Ether value attached to the function call.
  function contribute(address _address) payable returns (address, address, uint, uint){
    require(_address != address(0x0) && msg.value > 0 && flags[_address] == 1);
    Project currentProject = Project(_address);//Gets project instance
    require(currentProject.isActive());
    currentProject.fund.value(msg.value).gas(900000)(msg.sender);//This does the trick!!!
    LogContributionMade(msg.sender, _address, msg.value, _address.balance);
    return (msg.sender, _address, msg.value, _address.balance);
  }
  function getProjectList() constant returns (address[]) {
		return projects;
	}
  // Fallback();
  function ()
  {
    revert(); //  reverts state to before call
  }
}
