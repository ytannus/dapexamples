pragma solidity ^0.4.11;
contract Project {
  struct Data {                   //Project data:
    address owner;                //the address of the owner of the project
    uint    goal;                 //the amount to be raised
    uint    deadline;             //the time before which the amount has to be raised
    uint    raised;               //Amount reached
    bool    active;               //If project is active // BOOL Won't works!!!
  }
  Data public projectData;        //Project data storage
  mapping(address => uint) balances; //Sum of contributions made by a contributor
  
  mapping (address => uint) pendingWithdrawals; //from withdraw pattern

  //Events
  event LogCreateProject(address _owner, uint _goal, uint _deadline);
  event LogFundMade(address _contributor, uint _amount);
  event LogFailedFund(address _contributor, uint _amount);
  event LogPayout(address _owner, address _sender, uint _amount);
  event LogRefundMade(address _owner, uint _amount);
  event LogWithdrawMade(address _owner, uint _amount);
  
  //Modifiers
  modifier onlyOwner()
  {
    require(msg.sender == projectData.owner);
    _;
  }
  //Constructor
  function Project(address _owner, uint _goal, uint _deadline) {
    require(_owner != address(0));
    require(_goal > 0 && _deadline > now);
    projectData.active      = true;
    projectData.owner       = _owner;
    projectData.goal        = _goal;
    projectData.deadline    = _deadline;
    projectData.raised      = 0;
    LogCreateProject(_owner, _goal, _deadline);
  }
  //Methods
  function fund(address _contributor) payable returns (bool) {
    require(_contributor != address(0));
    require(msg.value > 0);
    assert(now < projectData.deadline);
    assert(projectData.active);
    assert(projectData.raised < projectData.goal);

    projectData.raised += msg.value;
    balances[_contributor] += msg.value;
    LogFundMade(_contributor, msg.value);
  }
 
  // refund()
  function refund() {
    require(balances[msg.sender] > 0);
    assert(projectData.active);//prevents from previous payout, closed projects
    assert(now >= projectData.deadline); //waits until deadline is over

    projectData.raised -= balances[msg.sender];
    pendingWithdrawals[msg.sender] = balances[msg.sender];
    balances[msg.sender] = 0;
    LogRefundMade(msg.sender,pendingWithdrawals[msg.sender]);
  }

  // payout()
  function payout() onlyOwner returns (bool) {
    assert(projectData.active);//prevents from previous payout
    assert(projectData.raised >= projectData.goal);
    assert(now >= projectData.deadline); //waits until deadline is over
    
    projectData.active = false; //Project gets inactive...closed now!
    pendingWithdrawals[projectData.owner] = projectData.raised;
    LogPayout(projectData.owner, msg.sender, pendingWithdrawals[projectData.owner]);
    return true;
  }

  // withdraw()
  function withdraw() {
    uint amount = pendingWithdrawals[msg.sender];
    pendingWithdrawals[msg.sender] = 0;
    msg.sender.transfer(amount);
    LogWithdrawMade(msg.sender, amount);
  }

  // Getters
  function isActive() constant returns (bool) {
    return projectData.active;
  }

  function getBalance() constant returns (uint) {
    return this.balance;
  }
  // Fallback();
  function ()
  {
    revert(); //reverts state to before call
  }

}
