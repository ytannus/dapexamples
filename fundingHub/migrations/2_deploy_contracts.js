var FundingHub = artifacts.require("./FundingHub.sol");
var initialGoal = 10; //Ethers
var initialDeadline = new Date(new Date().getTime()+1000*60*60*24).getTime()/1000; //1 day from now
module.exports = function(deployer) {
  //deployer.deploy(Project); // Project already imported by FundingHub contract
  deployer.deploy(FundingHub).then(function() {
    return FundingHub.deployed()
  }).then((instance) => {
    return instance.createProject(web3.toWei(initialGoal,"ether"), initialDeadline)
  }).then((tx) => {
    var date = new Date(initialDeadline*1000);
    console.log("====================================")
    console.log("Example Project created successfully")
    console.log("  Address : "+ tx.logs[0].args._projectAddress)
    console.log("  Goal    : "+ initialGoal);
    console.log("  Deadline: "+ date)
    console.log("====================================")
  });
};
