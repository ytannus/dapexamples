About the smart contracts:
1. In fund, wrongly checks that the contributor has a remaining balance bigger than what was sent. This has no practical purpose. "Oh so you want to buy this apple at 1$? But do you still have 1$ in your wallet after that?".
2. It is not necessary to do input sanitation in hub's createProject because the same is done in the project's constructor.
3. Unnecessarily keeps track of individual contributions in an array. This costs 3 SSTORE when only 1 would be necessary with a balances[] +=, which is done too BTW.
4. Keeping the timestamp at which the funding was done is not necessary and costs unnecessary gas. It can be found with the event.
5. The last to call fund and get over the goal will trigger payout. But payout checks whether it was called by the owner, and fails otherwise. This has the effect of reverting the last funding, and taking it back below the goal. Only if the owner submits the very last funding will it work.
6. The payout function checks if projectData.raised > 0. It is very weak compared to achieving the goal. This means that the owner can payout at any time. Contributors will feel this is close to being stolen.
7. The refund function takes an amount parameter. This is not necessary and only adds complexity.
8. The refund is only possible before the deadline. Possibly locking people out of their funds.
9. The refund allows theft of other people's Ether because the tested amount .amount >= _amount is not updated downward on successful refund so you can call again and again. If there were fewer things going on in the function it would have been more obvious.
10. In refund, the subtractions are not protected against potential wrap-around.
11. It would be silly, but it is possible to fund with 0.
12. The getProjectData getter is unnecessary because it returns a public struct, which already has a free getter.

About the migration files:
1. The synchronous call web3.eth.accounts should be avoid and instead accounts should be taken from module.exports = function(deployer, network, accounts) {.

About the test:
1. The deadline should not be hard-coded because some time in the future this test will fail because of it.
2. On line 26, there is an assert after a return. It is never reached.
3. Synchronous calls like web3.eth.getBalance should be avoided.
4. It is better to create a new hub in a beforeEach than reuse the .deployed() one.
5. Obviously mixed up weis (goal 10 weis) and ethers (contribution web3.toWei(3,"ether")). So this test should have been for a successful project that goes over the goal.
6. I got an Out-Of-Gas exception on the last it.
