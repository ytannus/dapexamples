import React, { Component } from 'react'
import FundingHubContract from '../build/contracts/FundingHub.json'
import ProjectContract from '../build/contracts/Project.json'

import getWeb3 from './utils/getWeb3'

import { HubInformation } from './components/HubInformation';
import { NewProjectForm }  from './components/NewProjectForm';
import { ProjectCard }  from './components/ProjectCard';

import './css/oswald.css'
import './css/open-sans.css'
import './css/pure-min.css'
import './App.css'

class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      web3: null,
      FundingHub: null,
      FundingHubAddress: 0,
      account: 0,
      accountBalance: 0,
      Project: null,
      ProjectList: [],
      ProjectsData: [],
      now: new Date().getTime()
    }

    this.handleInputChange = this.handleInputChange.bind(this)
    this.handleDateChange = this.handleDateChange.bind(this)
    this.handleNewProjectSubmit = this.handleNewProjectSubmit.bind(this)
    this.handleProjectAction = this.handleProjectAction.bind(this)
  }

  componentWillMount() {
    // Get network provider and web3 instance.
    // See utils/getWeb3 for more info.
    getWeb3
    .then(results => {
      const contract = require('truffle-contract')
      this.setState({
        web3: results.web3,
        FundingHub: contract(FundingHubContract),
        Project: contract(ProjectContract)
      })
      // Instantiate contract once web3 provided.
      this.instantiateContract()
      this.getProjectList()
    })
    .catch(() => {
      console.log('Error finding web3.')
    })
  }

  instantiateContract() {
      /*
       */
      this.state.FundingHub.setProvider(this.state.web3.currentProvider)
      this.state.Project.setProvider(this.state.web3.currentProvider)

      // Declaring this for later so we can chain functions on FundingHub.
      var FundingHubInstance

      // Get accounts.
      this.state.web3.eth.getAccounts((error, accounts) => {
        if (error != null) {
         console.log(error);
         alert("There was an error fetching your accounts.");
         return;
        }

        if (accounts.length === 0) {
          alert("Couldn't get any accounts! Make sure your Ethereum client is configured correctly.");
          return;
        }

        this.setState({ account: accounts[0] })

        this.state.FundingHub.deployed().then((instance) => {
          FundingHubInstance = instance
          this.setState({FundingHubInstance: instance})
          return FundingHubInstance.address
        }).then((address) => {
          return this.setState({FundingHubAddress: address})
        }).then((result) => {
          // Asynchronous getBalance.call is not working!!!
          // return fundingHubInstance.getBalance.call(this.state.account, {from: this.state.account})
          return this.getBalance(accounts[0])
        }).then((value) => {
          this.setState({ accountBalance: value})
        })
      })
    }

  async getBalance(addr) {
    const promisify = (inner) => new Promise((resolve, reject) => inner((err, res) => { if (err) { reject(err) } resolve(res) }))
    const getBalance = (addr, at) => promisify(cb => this.state.web3.eth.getBalance(addr, at, cb))
    const bigNumberBalance = await getBalance(addr)
    return this.state.web3.fromWei(bigNumberBalance.toNumber(),"ether")
  }
  /*
  async getBalance(addr) {
    let balance
    await this.state.FundingHub.deployed().then((instance) => {
      var value = this.state.web3.eth.getBalance(addr)
      return value
    }).then((value) => {
      balance = value
    })
    return this.state.web3.fromWei(balance.toNumber(),"ether")
  }
  */

  async createProject(goal,deadline) {
    let tx
    let addr

    await this.state.FundingHub.deployed().then((instance) => {
      //this.state.web3.personal.unlockAccount(this.state.account,passwd)
      tx = instance.createProject(goal,deadline/1000,{ from: this.state.account, gas:900000 })
      return tx
    }).then((tx) => {

      return (tx.logs[0].args._projectAddress)
    }).then((addr) => {
      this.setState({newProjectAddress: addr})
      this.getProjectList()
    }).catch((e) => {
      console.log(e)
    })
    return addr
  }

  async getProjectList() {
    //let list

    await this.state.FundingHub.deployed().then((instance) => {
      return instance.getProjectList.call()
    }).then((plist) => {
      this.setState({ ProjectList: plist })
    }).catch((e) => {
      console.log(e)
    })

    //Get project data
    this.updateProjectsData(this.state.ProjectList);
    //console.log(this.state.ProjectList)
  }

  async updateProjectsData(list) {
    let pdata = []

    for(var i=0;i < this.state.ProjectList.length;i++){
      await this.state.Project.at(this.state.ProjectList[i]).then((instance) => {
        return instance.projectData.call();
      }).then((data)=>{
        let item = {
          owner: data[0],
          goal: data[1],
          deadline: data[2]*1000,
          raised: data[3],
          active: data[4]
          }
        return item
      }).then((item) => {
        pdata.push(item)
      }).catch((e) => {
        console.log(e)
      })
    }
    this.setState({ProjectsData: pdata})
  }

  async contribute(address,amount) {
    let self = this
      await this.state.FundingHub.deployed().then((instance) => {
        return instance.contribute(address,{from: this.state.account, value: amount, gas:900000})
      }).then((tx) => {
         self.getProjectList()
         return this.getBalance(this.state.account)
      }).then((value) => {
          this.setState({ accountBalance: value})
      }).catch((e) => {
        console.log(e)
      })
  }

  async refund(address, amount) {
      await this.state.Project.at(address).then((instance) => {
        return instance.refund(amount, {from: this.state.account, gas:900000})
      }).then((tx) => {
         this.getProjectList()
         return this.getBalance(this.state.account)
      }).then((value) => {
          this.setState({ accountBalance: value})
      }).catch((e) => {
        console.log(e)
      })
  }

  async payout(address) {
      await this.state.Project.at(address).then((instance) => {
        return instance.payout({from: this.state.account, gas:900000})
      }).then((tx) => {
         this.getProjectList()
         return this.getBalance(this.state.account)
      }).then((value) => {
          this.setState({ accountBalance: value})
      }).catch((e) => {
        console.log(e)
      })
  }

  async withdraw(address) {
      await this.state.Project.at(address).then((instance) => {
        return instance.withdraw({from: this.state.account, gas:900000})
      }).then((tx) => {
         this.getProjectList()
         return this.getBalance(this.state.account)
      }).then((value) => {
          this.setState({ accountBalance: value})
      }).catch((e) => {
        console.log(e)
      })
  }

  //----- HANDLERS ------

  handleDateChange(date) {
    var unixtimestamp = Date.parse(date);
    this.setState({
      newProjectDeadline: date,
      UnixTS: unixtimestamp
    });
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  handleProjectAction(event) {
    event.preventDefault()
    var action = event.target.action.value
    var amount = event.target.amount.value
    var address = event.target.address.value

    switch(action) {
      case "contribute":
        this.contribute(address,amount)
        break
      case "refund":
        this.refund(address,amount)
        this.withdraw(address)
        break
      case "payout":
        this.payout(address)
        this.withdraw(address)
        break
      default:
        alert("No action detected")
        break

    }
  }

  handleNewProjectSubmit(event,deadline){
    event.preventDefault()
    //var passwd = event.target.accountPasswd.value
    var goal = this.state.web3.toWei(event.target.newProjectGoal.value,"ether")
    this.createProject(goal, deadline)
  }

  render() {
    return (
      <div className="App">

        <nav className="navbar pure-menu pure-menu-horizontal">
            <a href="#" className="pure-menu-heading pure-menu-link">Welcome to my FundingHub
            </a>
        </nav>

        <main className="container">
          <div className="pure-g">
            <div className="pure-u-1-4">
              <HubInformation
                FundingHubAddress={this.state.FundingHubAddress}
                account={this.state.account}
                accountBalance={this.state.accountBalance}
              />
              <NewProjectForm
                handleNewProjectSubmit={this.handleNewProjectSubmit}
              />
             </div>

            <div className="pure-u-3-4">
              <h2>Active Projects </h2>
              {
                this.state && this.state.ProjectList && this.state.ProjectsData &&
                this.state.ProjectsData.map((data,i) => {
                  if(data.active) {
                  return <ProjectCard key={this.state.ProjectList[i]} address={this.state.ProjectList[i]} data={data} accountBalance={this.state.accountBalance} handleProjectAction={this.handleProjectAction} account={this.state.account}/>
                  } else {
                    return false;
                  }
                })
              }
            </div>
          </div>
        </main>
      </div>
    );
  }
}

export default App
