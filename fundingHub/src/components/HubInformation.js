import React from 'react'
import '../css/pure-min.css'
import '../App.css'

export class HubInformation extends React.Component {
  render() {
    return (
      <div>
      <h3>Hub Information</h3>
        <div className="text-small pure-u-1-2">
           <div className="pure-u-1">
             <div className="pure-u-1-2 text-xsmall"><b>Hub Address</b></div>
             <div className="pure-u-1-2 text-xsmall">{this.props.FundingHubAddress}</div>
           </div>
           <div className="pure-u-1">
             <div className="pure-u-1-2 text-xsmall"><b>Account Address</b></div>
             <div className="pure-u-1-2 text-xsmall">{this.props.account}</div>
           </div>
           <div className="pure-u-1">
             <div className="pure-u-1-2 text-xsmall"><b>Account Balance (ether)</b></div>
             <div className="pure-u-1-2 text-xsmall">{this.props.accountBalance}</div>
           </div>
           <div className="pure-u-1">
              <span className="text-xsmall button-warning">Don&#39;t forget to unlock your account</span>
           </div>
         </div>

       </div>
    );
  }
}
