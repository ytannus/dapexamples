import React from 'react'
import '../css/pure-min.css'
import '../App.css'

import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';

export class NewProjectForm extends React.Component {
  constructor(props) {
    super(props)
      this.state = {
        newProjectGoal: 1,
        newProjectDeadline: moment(),
        newProjectAddress: "",
        newProjectStatus: "",
        UnixTS: 0
        //accountPasswd: ''
      }
      this.handleInputChange = this.handleInputChange.bind(this)
      this.handleDateChange = this.handleDateChange.bind(this)
      this.handleSubmit = this.handleSubmit.bind(this)
  }


  handleSubmit(event) {
    event.preventDefault()

    var goal = event.target.newProjectGoal.value
    var deadline = this.state.UnixTS
    //var passwd = event.target.accountPasswd.value
    var now = Date.now()

    if (goal <= 0) {
        this.setState({newProjectStatus: "Goal must be a positive integer..."})
      return
    }

    if (deadline <= now) {
      this.setState({newProjectStatus: "Project deadline must be in the future..."})
      return
    }
    this.setState({newProjectStatus: "Initiating transaction... (please wait)"})
    this.props.handleNewProjectSubmit(event,deadline)
    this.setState({newProjectStatus: "Project Created "})
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  handleDateChange(date) {
    var unixtimestamp = Date.parse(date);
    this.setState({
      newProjectDeadline: date,
      UnixTS: unixtimestamp
    });
  }

  render() {
    return (
      <div>
        <h3>Create new Project</h3>
        <form onSubmit={this.handleSubmit} className="pure-form pure-form-stacked">
          <div className="pure-u-1 pure-md-1-2">
           <label htmlFor="newProjectGoal" className="text-xsmall">Goal [ether]</label>
             <input
               className="text-xsmall"
               id="newProjectGoal"
               name="newProjectGoal"
               type="number"
               value={this.state.newProjectGoal}
               onChange={this.handleInputChange} required/>
           </div>
           <div className="pure-u-1 pure-md-1-2">
             <label htmlFor="newProjectDeadline" className="text-xsmall">Deadline</label>
               <DatePicker
                  className="text-xsmall"
                  id="newProjectDeadline"
                  locale="es"
                  name="newProjectDeadline"
                  selected={this.state.newProjectDeadline}
                  onChange={this.handleDateChange}
              />

              <span className="text-xsmall">{this.state.newProjectStatus}</span><br/>
              <span className="text-xsmall">{this.state.newProjectAddress}</span>
           </div>
         <button className="pure-button pure-button-primary">Create</button>
        </form>
      </div>
);
}
}
