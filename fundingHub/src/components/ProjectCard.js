import React from 'react'
import '../css/pure-min.css'
import '../App.css'

export class ProjectCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      amount: '',
      formStatus: ''
    }
    this.handleCardSubmit = this.handleCardSubmit.bind(this);
    this.handleActionChange = this.handleActionChange.bind(this);
  }
  handleCardSubmit(event) {
    event.preventDefault()
    let action = event.target.action.value
    var deadline = this.props.data.deadline.valueOf();
    var now = new Date().getTime();
    var amount = parseInt(event.target.amount.value,0)
    var raised = parseInt(this.props.data.raised,0)
    var goal = parseInt(this.props.data.goal,0)

    switch(action) {
      case "contribute":
        if (amount > this.props.accountBalance * 1000000000000000000) {
          this.setState({formStatus: "You don't have enough founds"})
          break
        }
        if (now > deadline) {
          this.setState({formStatus: "Deadline is over"})
          break
        }
        if (raised >= goal) {
          this.setState({formStatus: "Goal is reached"})
          break
        }
        //raised += amount;
        this.setState({formStatus: "Transaction sent to node, wait until mined"})
        this.props.handleProjectAction(event)
        break
      case "refund":
        if (now < deadline) {
          this.setState({formStatus: "You cannot refund yet, wait until deadline is over"})
          break
        }
        if (amount > raised) {
          this.setState({formStatus: "You can't refund more than raised amount"})
          break
        }       
        this.setState({formStatus: "Transaction sent to node, wait until mined"})
        this.props.handleProjectAction(event)
        break
      case "payout":
        if(this.props.account !== this.props.data.owner) {
          this.setState({formStatus: "You are not the project owner"})
          break
        }
        if (now < deadline) {
          this.setState({formStatus: "You cannot payout yet, wait until deadline is over"})
          break
        }
        this.setState({formStatus: "Project will be closed"})   
        this.props.handleProjectAction(event)
        break
      default:
        alert("No action detected")
        break
    }
  }

  handleActionChange(event) {
    event.preventDefault()
    if(event.target.value === "payout") {
      this.setState({amount: this.props.data.raised});
    }
  }

  showOwner(){
    return this.props.data.owner
  }
  showGoal(){
    let wei = this.props.data.goal.valueOf()
    let ether = parseFloat(wei / 1000000000000000000).toFixed(2)
    return (ether + " [ether] ("+wei+" [wei])")
  }
  showRaised(){
    let raised = this.props.data.raised.valueOf()
    let ether = parseFloat(raised / 1000000000000000000).toFixed(2)
    return (ether + " [ether] ("+raised+" [wei])")
  }

  showDeadline(timestamp){
    var date = new Date(timestamp*1);
    var now = new Date();
    var legend=(date < now)?" Expired":"";

    var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    return(date.getDate() + '/' + monthNames[(date.getMonth())] + '/' + date.getFullYear()+ legend);
  }

  render() {
    return (
        <div className="project-container">
          <table className="pure-table">
          <thead>
            <tr>
              <th colSpan="2"><span className="text-xsmall">{this.props.address}</span></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><span className="text-xsmall"><b>Owner</b></span></td>
              <td><span className="text-xsmall"> {this.props && this.props.data && this.props.data.owner && this.showOwner()}</span></td>
            </tr>
            <tr className="pure-table-odd">
              <td><span className="text-xsmall"><b>Goal</b></span></td>
              <td><span className="text-xsmall"> {this.props && this.props.data && this.props.data.goal && this.showGoal()}</span></td>
            </tr>
            <tr>
              <td><span className="text-xsmall"><b>Raised</b></span></td>
              <td><span className="text-xsmall"> {this.props && this.props.data && this.props.data.raised && this.showRaised()} </span></td>
            </tr>
            <tr className="pure-table-odd">
              <td><span className="text-xsmall"><b>Deadline</b></span></td>
              <td><span className="text-xsmall"> {this.props && this.props.data && this.props.data.deadline && this.showDeadline(this.props.data.deadline.valueOf())} </span></td>
            </tr>
            <tr>
              <td colSpan="2">
              <form className="pure-form" onSubmit={this.handleCardSubmit}>
                <input
                  id="amount"
                  type="number"
                  className="text-xsmall"
                  placeholder="amount in weis"
                  required
                />
                <select id="action" onChange={this.handleActionChange} className="text-xsmall">
                  <option defaultValue value="contribute">Contribute</option>
                  <option value="refund">Refund</option>
                  <option value="payout">Payout</option>
                </select>
                <input type="hidden" id="address" value={this.props.address}/>
                <button className="button-xsmall button-success pure-button">Submit</button>
              </form>

              </td>
            </tr>
            <tr>
              <td colSpan="2"><span className="text-xsmall">{this.state.formStatus}</span></td>
            </tr>
            </tbody>
          </table>
      </div>

    );
  }
}
