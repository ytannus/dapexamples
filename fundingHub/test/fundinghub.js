var FundingHub = artifacts.require("./FundingHub.sol");
var Project = artifacts.require("./Project.sol");

const addEvmFunctions = require("../src/utils/evmFunctions.js");
const addMinerFunctions = require("../src/utils/minerFunctions.js");

var Promise = require('bluebird');
Promise.allNamed = require("../src/utils/sequentialPromiseNamed.js");
Promise.retry = require("bluebird-retry");

addEvmFunctions(web3);
addMinerFunctions(web3);

if (typeof web3.eth.getBlockPromise !== "function") {
    Promise.promisifyAll(web3.eth, { suffix: "Promise" });
}
if (typeof web3.version.getNodePromise !== "function") {
    Promise.promisifyAll(web3.version, { suffix: "Promise" });
}
if (typeof web3.evm.increaseTimePromise !== "function") {
    Promise.promisifyAll(web3.evm, { suffix: "Promise" });
}
if (typeof web3.miner.startPromise !== "function") {
    Promise.promisifyAll(web3.miner, { suffix: "Promise" });
}


contract('FundingHub', function(accounts) {

  var _projectList = [];
  var _projectAddress;
  var _event;
  const _goal = 10;
  const _deadline = new Date(new Date().getTime()+1000*60*60*24).getTime()/1000; //1 day from now // FB3.3

//-----------------------------------------------------------------
  describe("Testing project creation", function() {

    it("Create Project", function() {
      return FundingHub.deployed().then(function(instance) {
        return instance.createProject(_goal, _deadline ,{from: accounts[0]});
      }).then((newProjectTx) => {
        _event = newProjectTx.logs[0].event;
        return newProjectTx.logs[0].args;
      }).then((args) => {
        _projectAddress = args._projectAddress;
      });
      assert.equal(_event,'LogCreateProject','Project was created at');
    });

    it("Check if project is stored in the Hub", function() {
      return FundingHub.deployed().then(function(instance) {
        return instance.getProjectList.call();
      }).then(list => {
        assert(list.indexOf(_projectAddress) != -1, true, "New project is inscribed in Hub list");
      })
    });

    it("Check new project initial values", function() {
      return Project.at(_projectAddress).then((projectInstance) => {
        return (projectInstance.projectData.call())
      }).then((data) => {
        console.log(data)
        assert.equal(data[0].toString(), accounts[0],"Owner should be account[0]");
        assert.equal(data[1].toString(), _goal,"Goal should be 10");
        assert.equal(data[2].toString(), parseInt(_deadline),"Deadline should be "+parseInt(_deadline));
        assert.equal(data[3].toString(), 0,"Raised should be 0");
        assert.equal(data[4].toString(), 'true',"Project is active");
      })
    })

  })
//-----------------------------------------------------------------
  describe("Testing contribute", function() {

    it("Check contribute 3 ether from accounts[0]", function() {
      return FundingHub.deployed().then(function(instance) {
        //Contribute 3 ether
        return instance.contribute(_projectAddress,{from: accounts[0], value: web3.toWei(3,"ether"), gas: 900000});
      }).then((tx) => {
        assert.equal(tx.logs[0].event, 'LogContributionMade',"Contribution made")
      })
    })

    it("Check project balance (3 ethers)", function() {
      return Project.at(_projectAddress).then((projectInstance) => {
        return (projectInstance.getBalance.call())
      }).then((balance) => {
        assert.equal(balance.toString(), web3.toWei(3,"ether"),"Project has 3 ethers");
      })
    })
  })
//-----------------------------------------------------------------
  describe("Testing refund",function() {
    it("Check contribute 2 ether from accounts[1]", function() {
      return FundingHub.deployed().then(function(instance) {
        //Contribute 3 ether
        return instance.contribute(_projectAddress,{from: accounts[1], value: web3.toWei(2,"ether"), gas: 900000});
      }).then((tx) => {
        assert.equal(tx.logs[0].event, 'LogContributionMade',"Contribution made")
      })
    })

    it("Check project balance (5 ethers)", function() {
      return Project.at(_projectAddress).then((projectInstance) => {
        return (projectInstance.getBalance.call())
      }).then((balance) => {
        assert.equal(balance.toString(), web3.toWei(5,"ether"),"Project has 5 ethers");
      })
    })

    //Go to the future
    it("Going to the future...(to deadline + 1 second) ", function() {
      return web3.eth.getBlockPromise("latest").then((block) => {
        return web3.evm.increaseTimePromise((parseInt(_deadline)) - block.timestamp + 3600*5)
        }).then(() => {
          return web3.evm.minePromise()
        }).then(() => {
          return web3.eth.getBlockPromise("latest")
        }).then((newblock) => {
          var date = new Date(newblock.timestamp*1000)
          assert.equal((newblock.timestamp - (parseInt(_deadline))) > 0, true,"We pass deadline")
        })
    })

    it("Refund 2 ethers from accounts[1]", function() {
      return Project.at(_projectAddress).then((projectInstance) => {
        return (projectInstance.refund({from: accounts[0], gas:900000}))
      }).then((tx) => {
        assert.equal(tx.logs[0].event, 'LogRefundMade',"Refund made")
      })
    })

    it("Withdraw (refund from accounts[1])", function() {
      return Project.at(_projectAddress).then((projectInstance) => {
        return (projectInstance.withdraw({from: accounts[1], gas:900000}))
      })
    })

    it("Check project balance (3 ethers)", function() {
      return Project.at(_projectAddress).then((projectInstance) => {
        return (projectInstance.getBalance.call())
      }).then((balance) => {
        assert.equal(balance.toString(), web3.toWei(3,"ether"),"Project has 3 ethers");
      })
    })

    it("Check project raised value (3 ethers)", function() {
      return Project.at(_projectAddress).then((projectInstance) => {
        return (projectInstance.projectData.call())
      }).then((data) => {
        assert.equal(data[3].toString(), web3.toWei(3,"ether"),"Raised should be 3 ethers");
      })
    })

  })

//-----------------------------------------------------------------
  describe("Testing payout", function() {

    it("Check payout()", function() {
      return Project.at(_projectAddress).then((projectInstance) => {
        return (projectInstance.payout({from: accounts[0]}))
      }).then((tx) => {
        assert.equal(tx.logs[0].event, 'LogPayout',"Payout made")
      })
    })
    it("Withdraw (payout)", function() {
      return Project.at(_projectAddress).then((projectInstance) => {
        return (projectInstance.withdraw({from: accounts[0]}))
      })
    })

    it("Check project balance (0 ethers)", function() {
      return Project.at(_projectAddress).then((projectInstance) => {
        return (projectInstance.getBalance.call())
      }).then((balance) => {
        assert.equal(balance.toString(), web3.toWei(0,"ether"),"Project has 0 ethers");
      })
    })
    it("Check project raised value (3 ethers)", function() {
      return Project.at(_projectAddress).then((projectInstance) => {
        return (projectInstance.projectData.call())
      }).then((data) => {
        assert.equal(data[3].toString(), web3.toWei(3,"ether"),"Raised should be 3 ethers");
      })
    })
    it("Check project closed", function() {
      return Project.at(_projectAddress).then((projectInstance) => {
        return (projectInstance.projectData.call())
      }).then((data) => {
        assert.equal(data[4].toString(), 'false',"Project is closed");
      })
    })

  })
//-----------------------------------------------------------------


});//contract
